<?php

namespace Deployer;

require 'recipe/symfony.php';


// Project name
set('application', 'palapala');

// Project repository
set('repository', 'git@gitlab.com:vittore.zen/palapala.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', [
    '.env',
    '.env.local'
]);
set('shared_dirs', [
    'var/log', 'files-repository'
]);

// Writable dirs by web server
set('writable_dirs', [
    'var/cache', 'files-repository',
    'var/log',
]);

set('composer_options', ' --verbose --prefer-dist --no-progress --no-interaction --no-scripts --no-dev --optimize-autoloader');


set('http_user', 'www-data');
set('symfony_env', 'prod');

set('bin_dir', 'bin');
set('var_dir', 'app');
// Servers

set('keep_releases', 3);
#set('writable_mode', 'chmod');

// Hosts
set('remote_user', 'root');

// Hosts
import('hosts.yaml');


// Build yarn locally
task('deploy:build:assets', function (): void {
    runlocally('yarn install');
    runlocally('yarn encore production');
})->once()->desc('Install front-end assets');

before('deploy:symlink', 'deploy:build:assets');

// Upload assets
task('upload:assets', function (): void {
    upload(__DIR__ . '/public/assets/', '{{release_path}}/public/assets');
    run('cd {{release_path}} &&  {{bin/console}} assets:install');
});

after('deploy:build:assets', 'upload:assets');

task('deploy:build-api-doc', function (): void {
    runlocally('php bin/console nelmio:apidoc:dump --format=html > public/api.html');
    upload(__DIR__ . '/public/api.html', '{{release_path}}/public/api.html');
})->desc('Create API documentation');

after('upload:assets', 'deploy:build-api-doc');


// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'database:migrate');


task('deploy:elastica', function (): void {
    run('cd {{deploy_path}} &&  {{bin/console}} fos:elastica:populate');
});

after('deploy:symlink', 'deploy:elastica');




