echo "cache:clear and cache:warmup"
php bin/console cache:clear --env=test --no-interaction --no-debug
echo "doctrine:database:drop"
php bin/console doctrine:database:drop --force --env=test --if-exists --no-interaction --no-debug
echo "doctrine:database:create"
php bin/console doctrine:database:create --env=test --if-not-exists --no-interaction --no-debug
echo "doctrine:schema:update"
php bin/console doctrine:schema:update --complete --force --env=test --no-interaction
#echo "cache:warmup"
#php bin/console cache:warmup --env=test --no-debug --no-interaction
echo "doctrine:fixtures:load"
php bin/console doctrine:fixtures:load --env=test --no-debug --no-interaction
echo "fos:elastic:populate"
php bin/console fos:elastic:populate --env=test --no-debug --no-interaction


