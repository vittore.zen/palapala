#!/bin/bash

vendor/bin/phpstan analyse src  --memory-limit 1G -l 6
vendor/bin/codecept run