require('jquery');

const imagesContext = require.context('../images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);


import "@fortawesome/fontawesome-free/js/all";

const $ = require('jquery');
global.$ = global.jQuery = $;
require('jquery-ui');
require('jquery-ui/ui/widgets/sortable');

require('../scss/bootstrap-custom.scss');
window.bootstrap = require('bootstrap');
import {Tooltip, Toast, Popover} from 'bootstrap';
require('@popperjs/core');



import Autocomplete from '@trevoreyre/autocomplete-js'

global.Autocomplete = Autocomplete;

require("select2");

import '@pugx/filter-bundle/js/filter';

import Cookies from 'js-cookie';
global.Cookies = Cookies;

$(document).ready(function () {
    'use strict';

    if (jQuery().pugxFilter) {
        $('#filter').pugxFilter();
    }

    /** select2 no focus workaround **/
    $(document).on('select2:open', () => {
        let allFound = document.querySelectorAll('.select2-container--open .select2-search__field');
        $(this).one('mouseup keyup', () => {
            setTimeout(() => {
                allFound[allFound.length - 1].focus();
            }, 0);
        });
    });

    /** enable popover everywhere **/
    var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
    var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
        return new bootstrap.Popover(popoverTriggerEl)
    })

});
