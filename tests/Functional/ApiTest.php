<?php

namespace App\Tests\Functional\Functional;


use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiTest extends WebTestCase
{
    public function testSimpleSearch(): void
    {
        $client = static::createClient();
        $client->request('GET', '/api/search/?q=loghetto tesina');
        $this->assertResponseIsSuccessful();
        $this->assertJsonStringEqualsJsonString('[
  {
    "score": 0.76253086,
    "id": "1e0c0020-a201-45ef-8d2b-78550dc9d734",
    "title": "F2",
    "description": "Logo tesi",
    "type": "folder"
  }
]', $client->getResponse()->getContent());
    }
}
