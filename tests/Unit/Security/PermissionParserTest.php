<?php

declare(strict_types=1);
namespace App\Tests\Unit\Security;

use App\Entity\Document;
use App\Entity\Folder;
use App\Entity\Link;
use App\Entity\User;
use App\Security\PermissionParser;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


final class PermissionParserTest extends KernelTestCase
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var App\Security\PermissionParser
     */
    private $permissionParser;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $container = static::getContainer();
        $this->entityManager = $container->get('doctrine')->getManager();
        $this->permissionParser = $container->get(PermissionParser::class);
        $loader = new Loader();
        $loader->loadFromDirectory('src/DataFixtures');
        $executor = new ORMExecutor($this->entityManager, new ORMPurger());
        $executor->execute($loader->getFixtures());
    }

    /*
                                                     no-auth     user1      user2 (G1)   user3(G1,G2)
        ROOT E:V                                        V          V          V            V
         |
         +-- F1 E:E                                     E          E          E            E
         |
         +-- F2 (E:V)                                   V          V          V            V
         |
         +-- F3 Auth:E G1:E (E:V)                       V          E          E            E
             |
             +-- F4 E:NA Auth:V (G1:E)                  NA         V          E            E
             |
             +-- F5 (E:V Auth:E G1:E)                   V          E          E            E
                 |
                 +-- F6 G2:E (E:V Auth:E G1:E)          V          E          E            E
                     |
                     +-- F7 G2:V (E:V Auth:E G1:E)      V          E          E            V
     */
    public function testRoot(): void
    {
        $this->assertSame('view', $this->permissionParser->parse(
            $this->getFolder('*** ROOT ***'), null
        ));
        $this->assertSame('view', $this->permissionParser->parse(
            $this->getFolder('*** ROOT ***'), $this->getUser('user1')
        ));
        $this->assertSame('view', $this->permissionParser->parse(
            $this->getFolder('*** ROOT ***'), $this->getUser('user2')
        ));
        $this->assertSame('view', $this->permissionParser->parse(
            $this->getFolder('*** ROOT ***'), $this->getUser('user3')
        ));
    }

    /*
                                                 no-auth     user1      user2 (G1)   user3(G1,G2)
     +-- F1 E:E                                     E          E          E            E
    */
    public function testF1(): void
    {
        $this->assertSame('edit', $this->permissionParser->parse(
            $this->getFolder('F1'), null
        ));
        $this->assertSame('edit', $this->permissionParser->parse(
            $this->getFolder('F1'), $this->getUser('user1')
        ));
        $this->assertSame('edit', $this->permissionParser->parse(
            $this->getFolder('F1'), $this->getUser('user2')
        ));
        $this->assertSame('edit', $this->permissionParser->parse(
            $this->getFolder('F1'), $this->getUser('user3')
        ));
    }

    /*                                                 no-auth     user1      user2 (G1)   user3(G1,G2)
           +-- F2 (E:V)                                   V          V          V            V
    */
    public function testF2(): void
    {
        $this->assertSame('view', $this->permissionParser->parse(
            $this->getFolder('F2'), null
        ));
        $this->assertSame('view', $this->permissionParser->parse(
            $this->getFolder('F2'), $this->getUser('user1')
        ));
        $this->assertSame('view', $this->permissionParser->parse(
            $this->getFolder('F2'), $this->getUser('user2')
        ));
        $this->assertSame('view', $this->permissionParser->parse(
            $this->getFolder('F2'), $this->getUser('user3')
        ));
    }

    /*                                                 no-auth     user1      user2 (G1)   user3(G1,G2)
           +-- F3 Auth:E G1:E (E:V)                       V          E          E            E
    */
    public function testF3(): void
    {
        $this->assertSame('view', $this->permissionParser->parse(
            $this->getFolder('F3'), null
        ));
        $this->assertSame('edit', $this->permissionParser->parse(
            $this->getFolder('F3'), $this->getUser('user1')
        ));
        $this->assertSame('edit', $this->permissionParser->parse(
            $this->getFolder('F3'), $this->getUser('user2')
        ));
        $this->assertSame('edit', $this->permissionParser->parse(
            $this->getFolder('F3'), $this->getUser('user3')
        ));
    }

    /*
                                                 no-auth     user1      user2 (G1)   user3(G1,G2)
         +-- F4 E:NA Auth:V                         NA         V          V            V
     */
    public function testF4(): void
    {
        $this->assertSame('no-access', $this->permissionParser->parse(
            $this->getFolder('F4'), null
        ));
        $this->assertSame('view', $this->permissionParser->parse(
            $this->getFolder('F4'), $this->getUser('user1')
        ));
        $this->assertSame('view', $this->permissionParser->parse(
            $this->getFolder('F4'), $this->getUser('user2')

        ));
        $this->assertSame('view', $this->permissionParser->parse(
            $this->getFolder('F4'), $this->getUser('user3')

        ));
    }

    /*
                                                     no-auth     user1      user2 (G1)   user3(G1,G2)
             +-- F5 (E:V Auth:E G1:E)                   V          E          E            E
     */
    public function testF5(): void
    {
        $this->assertSame('view', $this->permissionParser->parse(
            $this->getFolder('F5'), null
        ));
        $this->assertSame('edit', $this->permissionParser->parse(
            $this->getFolder('F5'), $this->getUser('user1')
        ));
        $this->assertSame('edit', $this->permissionParser->parse(
            $this->getFolder('F5'), $this->getUser('user2')
        ));
        $this->assertSame('edit', $this->permissionParser->parse(
            $this->getFolder('F5'), $this->getUser('user3')

        ));
    }

    /*
                                                 no-auth     user1      user2 (G1)   user3(G1,G2)
             +-- F6 G2:E (E:V Auth:E G1:E)          V          E          E            E
     */
    public function testF6(): void
    {
        $this->assertSame('view', $this->permissionParser->parse(
            $this->getFolder('F6'), null
        ));
        $this->assertSame('edit', $this->permissionParser->parse(
            $this->getFolder('F6'), $this->getUser('user1')
        ));
        $this->assertSame('edit', $this->permissionParser->parse(
            $this->getFolder('F6'), $this->getUser('user2')
        ));
        $this->assertSame('edit', $this->permissionParser->parse(
            $this->getFolder('F6'), $this->getUser('user3')

        ));
    }

    /*
                                                 no-auth     user1      user2 (G1)   user3(G1,G2)
                 +-- F7 G2:V (E:V Auth:E G1:E)      V          E          E            V
     */
    public function testF7(): void
    {
        $this->assertSame('view', $this->permissionParser->parse(
            $this->getFolder('F7'), null
        ));
        $this->assertSame('edit', $this->permissionParser->parse(
            $this->getFolder('F7'), $this->getUser('user1')
        ));
        $this->assertSame('edit', $this->permissionParser->parse(
            $this->getFolder('F7'), $this->getUser('user2')
        ));
        $this->assertSame('view', $this->permissionParser->parse(
            $this->getFolder('F7'), $this->getUser('user3')

        ));
    }

    /*                                             no-auth     user1      user2 (G1)   user3(G1,G2)
     *  |    +-- L1 (E:E)                              E          E          E            E
     */
    /**
     * @return void
     */

    public function testL1(): void
    {
        $object=$this->getLink('L1');
        $this->assertSame('edit', $this->permissionParser->parse($object, null));
        $this->assertSame('edit', $this->permissionParser->parse($object,  $this->getUser('user1')));
        $this->assertSame('edit', $this->permissionParser->parse($object, $this->getUser('user2')));
        $this->assertSame('edit', $this->permissionParser->parse($object, $this->getUser('user3')));
    }



    /*                                             no-auth     user1      user2 (G1)   user3(G1,G2)
     *  |    +-- D1 (E:E)                              E          E          E            E
     */
    /**
     * @return void
     */

    public function testD1(): void
    {
        $object=$this->getDocument('D1');
        $this->assertSame('edit', $this->permissionParser->parse($object, null));
        $this->assertSame('edit', $this->permissionParser->parse($object,  $this->getUser('user1')));
        $this->assertSame('edit', $this->permissionParser->parse($object, $this->getUser('user2')));
        $this->assertSame('edit', $this->permissionParser->parse($object, $this->getUser('user3')));
    }



    /*                                             no-auth     user1      user2 (G1)   user3(G1,G2)
     *  |    +-- L2 Auth:E (E:V)                       V          E          E            E
     */
    /**
     * @return void
     */

    public function testL2(): void
    {
        $object=$this->getLink('L2');
        $this->assertSame('view', $this->permissionParser->parse($object, null));
        $this->assertSame('edit', $this->permissionParser->parse($object,  $this->getUser('user1')));
        $this->assertSame('edit', $this->permissionParser->parse($object, $this->getUser('user2')));
        $this->assertSame('edit', $this->permissionParser->parse($object, $this->getUser('user3')));
    }


    /*                                             no-auth     user1      user2 (G1)   user3(G1,G2)
     *  |    +-- D2 Auth:E (E:V)                       V          E          E            E
     */
    /**
     * @return void
     */

    public function testD2(): void
    {
        $object=$this->getDocument('D2');
        $this->assertSame('view', $this->permissionParser->parse($object, null));
        $this->assertSame('edit', $this->permissionParser->parse($object,  $this->getUser('user1')));
        $this->assertSame('edit', $this->permissionParser->parse($object, $this->getUser('user2')));
        $this->assertSame('edit', $this->permissionParser->parse($object, $this->getUser('user3')));
    }


    /*                                             no-auth     user1      user2 (G1)   user3(G1,G2)
     *  |    +-- L3 G1:E (E:V)                         V          V          E            E
     */
    /**
     * @return void
     */

    public function testL3(): void
    {
        $object=$this->getLink('L3');
        $this->assertSame('view', $this->permissionParser->parse($object, null));
        $this->assertSame('view', $this->permissionParser->parse($object,  $this->getUser('user1')));
        $this->assertSame('edit', $this->permissionParser->parse($object, $this->getUser('user2')));
        $this->assertSame('edit', $this->permissionParser->parse($object, $this->getUser('user3')));
    }


    /*                                             no-auth     user1      user2 (G1)   user3(G1,G2)
     *  |    +-- D3 G1:E (E:V)                         V          V          E            E
     */
    /**
     * @return void
     */

    public function testD3(): void
    {
        $object=$this->getDocument('D3');
        $this->assertSame('view', $this->permissionParser->parse($object, null));
        $this->assertSame('view', $this->permissionParser->parse($object,  $this->getUser('user1')));
        $this->assertSame('edit', $this->permissionParser->parse($object, $this->getUser('user2')));
        $this->assertSame('edit', $this->permissionParser->parse($object, $this->getUser('user3')));
    }



    private function getFolder(string $title): ?Folder
    {
        return $this->entityManager->getRepository(Folder::class)->findOneByTitle($title);
    }

    private function getLink(string $title): ?Link
    {
        return $this->entityManager->getRepository(Link::class)->findOneByTitle($title);
    }

    private function getDocument(string $title): ?Document
    {
        return $this->entityManager->getRepository(Document::class)->findOneByTitle($title);
    }
    private function getUser(string $username): ?User
    {
        return $this->entityManager->getRepository(User::class)->findOneByUsername($username);
    }
}
