<?php declare(strict_types=1);

namespace App\Tests\Unit\Entity;

use App\DataFixtures\UserFixture;
use App\Entity\Folder;
use App\Entity\User;
use App\Entity\Group;
use App\Entity\GroupAcl;
use App\Entity\Security;
use App\DataFixtures\FolderFixture;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Security\PermissionParser;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Component\Uid\Uuid;

class FolderTest extends KernelTestCase
{


    public function testGettersAndSetters(): void
    {

        $folder1 = new Folder();
        $folder1->setTitle('T1');
        $this->assertEquals('T1', $folder1->getTitle());
        $this->assertEquals('T1', $folder1->__toString());
        $folder1->setDescription('d1');
        $this->assertEquals('d1', $folder1->getDescription());
        $folder1->setSlug('t1bis');
        $this->assertEquals('t1bis', $folder1->getSlug());
        $t1 = new \DateTime();
        $folder1->setCreated($t1);
        $this->assertEquals($t1->format('D, d M Y H:i:s'), $folder1->getCreated()->format('D, d M Y H:i:s'),);
        $t2 = new \DateTime();
        $folder1->setUpdated($t2);
        $this->assertEquals($t2->format('D, d M Y H:i:s'), $folder1->getUpdated()->format('D, d M Y H:i:s'),);
        $this->assertNull($folder1->getParent());
        

        $folder2 = new Folder();
        $folder2->setTitle('T2');
        $folder1->addChild($folder2);
        $this->assertCount(1,$folder1->getChildren());

        $this->assertEquals($folder1->getChildren()[0]->getId(), $folder2->getId(),'Child not found');




$folder1->removeChild($folder2);
$this->assertCount(0,$folder1->getChildren());


    }
}
