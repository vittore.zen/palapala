<?php declare(strict_types=1);

namespace App\Tests\Unit\Entity;

use App\DataFixtures\UserFixture;
use App\Entity\Folder;
use App\Entity\User;
use App\Entity\Group;
use App\Entity\GroupAcl;
use App\Entity\Security;
use App\DataFixtures\FolderFixture;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Security\PermissionParser;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Component\Uid\Uuid;

class GroupTest extends KernelTestCase
{


    public function testGettersAndSetters():void
    {

        $group = new Group();
        $group->setName('G1');
        $this->assertInstanceOf(Uuid::class, $group->getId());
        $this->assertEquals('G1', $group->getName());
        $this->assertEquals('G1', $group->__toString());


        $user = new User();
        $user->setUsername('john_doe');
        $this->assertEquals('john_doe', $user->getUsername());
        $this->assertEquals('john_doe', $user->__toString());
        $this->assertEquals('john_doe', $user->getUserIdentifier());
        $user->setPassword('password123');
        $this->assertEquals('password123', $user->getPassword());
        $user->setPlainPassword('password123');
        $this->assertEquals('password123', $user->getPlainPassword());
        $user->setLastname('last');
        $this->assertEquals('last', $user->getLastname());
        $user->setEmail('email');
        $this->assertEquals('email', $user->getEmail());
        $user->setFirstname('first');
        $this->assertEquals('first', $user->getFirstname());
        $user->setPasswordRequestToken('123');
        $this->assertEquals('123', $user->getPasswordRequestToken());
        $user->setRoles(['ROLE_SPECIAL']);
        $this->assertEquals(['ROLE_SPECIAL'], $user->getRoles());
        $this->assertInstanceOf(Uuid::class, $user->getId());
        $this->assertEquals('G1', $group->getName());
        $this->assertEquals('G1', $group->__toString());
        $t1 = new \DateTime();
        $user->setCreated($t1);
        $this->assertEquals($t1->format('D, d M Y H:i:s'), $user->getCreated()->format('D, d M Y H:i:s'),);
        $t2 = new \DateTime();
        $user->setUpdated($t2);
        $this->assertEquals($t2->format('D, d M Y H:i:s'), $user->getUpdated()->format('D, d M Y H:i:s'),);
        $t3 = new \DateTime();
        $user->setLastLogin($t3);
        $this->assertEquals($t3->format('D, d M Y H:i:s'), $user->getLastLogin()->format('D, d M Y H:i:s'),);
        $group->addUser($user);
        $this->assertNotNull($group->getUsers());
        $this->assertInstanceOf(User::class,$group->getUsers()[0]);
        $this->assertInstanceOf(Group::class,$user->getGroups()[0]);
        $this->assertEquals($user->getId(),$group->getUsers()[0]->getId());
        $group->removeUser($user);
        $this->assertSameSize([],$group->getUsers());
        $user->eraseCredentials();
        $this->assertNull( $user->getPlainPassword());

    }
}
