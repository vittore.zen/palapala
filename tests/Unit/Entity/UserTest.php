<?php declare(strict_types=1);

namespace App\Tests\Unit\Entity;

use App\DataFixtures\UserFixture;
use App\Entity\Folder;
use App\Entity\User;
use App\Entity\Group;
use App\Entity\GroupAcl;
use App\Entity\Security;
use App\DataFixtures\FolderFixture;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Security\PermissionParser;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Component\Uid\Uuid;

class UserTest extends KernelTestCase
{


    public function testGettersAndSetters()
    {
        // Creazione di un nuovo utente
        $user = new User();
        $user->setUsername('john_doe');
        $user->setPassword('password123');
        $user->setPlainPassword('password123');
        $user->setLastname('last');
        $user->setEmail('email');
        $user->setFirstname('first');
        $user->setPasswordRequestToken('123');
        $user->setRoles(['ROLE_SPECIAL']);
        $t1 = new \DateTime();
        $user->setCreated($t1);
        $t2 = new \DateTime();
        $user->setUpdated($t2);
        $t3 = new  \DateTime();
        $user->setLastLogin($t3);

        // Verifica dei getter
        $this->assertInstanceOf(Uuid::class,$user->getId());

        $this->assertEquals('john_doe', $user->getUsername());
        $this->assertEquals('password123', $user->getPassword());
        $this->assertEquals('password123', $user->getPlainPassword());
        $this->assertEquals('last', $user->getLastname());
        $this->assertEquals('email', $user->getEmail());
        $this->assertEquals('first', $user->getFirstname());
        $this->assertEquals('123', $user->getPasswordRequestToken());
        $this->assertEquals('ROLE_SPECIAL', $user->getRoles()[0]);
        $this->assertEquals($user->getUsername(), $user->__toString());
        $this->assertEquals($user->getUsername(), $user->getUserIdentifier());
        $this->assertEquals(null, $user->getSalt());
        $this->assertEquals($t1, $user->getCreated());
        $this->assertEquals($t2, $user->getUpdated());
        $this->assertEquals($t3, $user->getLastLogin());


        $user->eraseCredentials();
        $this->assertNull( $user->getPlainPassword());


        $group=new Group();
        $group->setName('G1');

        $user->addGroup($group);

        $this->assertNotNull($user->getGroups());
        $g=$user->getGroups()[0];
        $this->assertInstanceOf(Group::class,$g);

        $this->assertEquals('G1',$g->getName());

        $user->removeGroup($group);
        $this->assertSameSize([],$user->getGroups());


    }
}
