<?php declare(strict_types=1);

namespace App\Tests\Unit\Entity;

use App\DataFixtures\UserFixture;
use App\Entity\Folder;
use App\Entity\Link;
use App\Entity\User;
use App\Entity\Group;
use App\Entity\GroupAcl;
use App\Entity\Security;
use App\DataFixtures\FolderFixture;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Security\PermissionParser;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Component\Uid\Uuid;

class LinkTest extends KernelTestCase
{


    public function testGettersAndSetters():void
    {
        $object = new Link();
        $object->setTitle('LX');
        $object->setDescription('Description LX');
        $object->setUrl('URL of LX');
        $this->assertInstanceOf(Uuid::class, $object->getId());
        $this->assertEquals('LX', $object->getTitle());
        $this->assertEquals('LX', $object->__toString());
        $this->assertEquals('Description LX', $object->getDescription());
        $this->assertEquals('URL of LX', $object->getURL());
        $t1 = new \DateTime();
        $object->setCreatedAt($t1);
        $this->assertEquals($t1->format('D, d M Y H:i:s'), $object->getCreatedAt()->format('D, d M Y H:i:s'),);
        $t2 = new \DateTime();
        $object->setUpdatedAt($t2);
        $this->assertEquals($t2->format('D, d M Y H:i:s'), $object->getUpdatedAt()->format('D, d M Y H:i:s'),);
    }
}
