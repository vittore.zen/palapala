<?php

use Symfony\Component\Dotenv\Dotenv;
use Doctrine\Common\DataFixtures\Loader;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;


require dirname(__DIR__).'/vendor/autoload.php';




if (file_exists(dirname(__DIR__).'/config/bootstrap.php')) {
    require dirname(__DIR__).'/config/bootstrap.php';
} elseif (method_exists(Dotenv::class, 'bootEnv')) {
    (new Dotenv())->bootEnv(dirname(__DIR__).'/.env');
}


/*

function bootstrap(): void
{
    echo "\nbootstrap: kernel boot\n";
    $kernel = new \App\Kernel('test', true);
    $kernel->boot();
    echo "\nbootstrap: application boot\n";
    $application = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
    $application->setAutoExit(false);
/*
    echo "\ndoctrine: load src/DataFixtures\n";
    $loader = new Loader();
    $loader->loadFromDirectory('src/DataFixtures');

    $cmd = ['php', 'bin/console', 'fos:elastic:populate', '--env=test'];
    echo "\nrun: fos:elastic:populate\n";
    $process = new Process($cmd);
    $process->run(function ($type, $buffer) {
        if (Process::ERR === $type) {
            echo 'ERR > '.$buffer;
        } else {
            echo 'OUT > '.$buffer;
        }
    });



//    $kernel->getContainer()->get('doctrine')->getConnection()->executeQuery('CREATE TABLE test (test VARCHAR(10))');
    $kernel->shutdown();
}

bootstrap();

*/