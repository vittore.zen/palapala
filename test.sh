#!/usr/bin/env bash

vagrant ssh-config > vagrant-ssh
ssh -F vagrant-ssh default "cd /vagrant;./scripts/before-test-run.sh"
ssh -F vagrant-ssh default "cd /vagrant; php -dpcov.enabled=1 -dpcov.directory=. -dpcov.exclude='~vendor~' ./vendor/bin/phpunit --testdox --coverage-text  --coverage-html tests/_build/output/coverage $1"
rm vagrant-ssh
