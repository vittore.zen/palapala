# palapala

We needed a simple and flexible document management to be included in the CMS we use. We didn't find anything we liked that was simple so we did it for us and for you.
```palapala``` allows you to make your documents available on your website with ease and with an efficient search system.

*Note:* this project is under active development, use it carefully in production.


## Setup for production
### Prerequisites
If you take a look inside ```provisioning\provision.sh``` you can find all information to prepare your system. This file is for debian 10/12, our preferred platforms.

## Setup
1. Download and unzip (https://gitlab.com/vittore.zen/palapala/-/archive/main/palapala-main.zip)[https://gitlab.com/vittore.zen/palapala/-/archive/main/palapala-main.zip]
2. Open a terminal session and inside palapala directory run ```composer install```. This operation download all libraries inside ```vendor``` folder.
3. Open in a editor ```.env.local```. This file is main configuration file. Everything you need is inside it. 
4. Create database and schema:
```
php bin/console doctrine:database:create
ophp bin/console doctrine:schema:update --force
```
5. Compile frontend:
```
yarn install
yarn encore prod
```
6. Open browser at palapala web site.
7. Api documentation ```http:\\<host ip>\api\doc```


## Setup for development
We use [Vagrant](https://www.vagrantup.com/) for development.
1. Copy ```Vagrantfile.virtualbox.dist``` into ```Vagrantfile``` and change it according to your platform
2. Start with ```vagrant up```
3. Run test:
```./test.sh```
Coverage results are in ```tests/_build/output/coverage```.




## Why ```palapala```?
The word *palapala* in Hawaiian means _document_ and in Italian it sounds like _shovel-shovel_. 

----
# Useful ```php bin/console``` commands
```user:create```                                Create user
```user:password-reset```                        Change password
```user:promote```                               Promote an user to admin role
```check-permission```                           Check user permission of an object (folder, document or link)
```doctrine:fixtures:load```                     Load data fixtures to your database
```import:remository-documents```                Import from remository joomla plugin
```import:remository-folders```                  Import from remository joomla plugin
