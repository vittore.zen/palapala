#!/usr/bin/env bash

remote=10.10.240.29


docker compose down
docker compose up --pull always -d
ssh root@$remote 'cd /tmp;sudo -u postgres pg_dump  dbname=iusve_docs > /tmp/tmp.sql'
scp root@$remote:/tmp/tmp.sql tmp.sql
docker compose exec  php bin/console doctrine:database:drop --force
docker compose exec  php bin/console doctrine:database:create

cat tmp.sql| docker  exec -i  palapala-database-1 psql app app
rm tmp.sql


rsync -avz root@$remote:/var/www/iusve.it/docs/current/files-repository/* files-repository --delete

dphp bin/console fos:elastica:populate
