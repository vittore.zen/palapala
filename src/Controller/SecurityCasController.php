<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityCasController extends AbstractController
{
    #[Route(path: '/login', name: 'login', methods: ['GET', 'POST'])]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {

        return $this->render(
            'login_logout/login.html.twig',
            [
            ]
        );
    }

    #[Route(path: '/cas/not-authorized', name: 'cas-not-authorized', methods: ['GET', 'POST'])]
    public function notAutorized(AuthenticationUtils $authenticationUtils): Response
    {
        return $this->render(
            'login_logout/cas_not_authorized.html.twig',
            [
            ]
        );
    }
}
