<?php

namespace App\Controller;

use App\Entity\Folder;
use App\Entity\Group;
use App\Entity\GroupAcl;
use App\Entity\Security;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'homepage')]
    public function homepage(): Response
    {
        return $this->redirectToRoute('folders');
    }

    #[Route('/folders/{folderId?}', name: 'folders')]
    public function folders(EntityManagerInterface $entityManager, ?string $folderId): Response
    {
        $this->setupNewForest($entityManager);
        $folderRoot = $entityManager->getRepository(Folder::class)->findOneBy(['title'=>'*** ROOT ***']);
        if (null == $folderId) {
            $folderId = $folderRoot->getId();
        }
        try {
            $folder = $entityManager->getRepository(Folder::class)->find($folderId);
        } catch (ConversionException $e) {
            $folder = $entityManager->getRepository(Folder::class)->findOneBy(['slug'=>$folderId]);
        }
        if ($folderRoot->getId() != $folderId && !$this->isGranted('view', $folder)) {
            throw new \LogicException('Invalid folder permissions');
        }
        $folderTrash = $entityManager->getRepository(Folder::class)->findOneBy(['title'=> '*** TRASH ***']);
        if ($folder->getId() == $folderTrash->getId()) {
            return $this->render('default/trash.html.twig', [
                'currentFolder' => $folder,
                'path' => $entityManager->getRepository(Folder::class)->getPath($folder),
                'folderTrash' => $folderTrash,
                'folderRoot' => $folderRoot,
            ]);
        }

        return $this->render('default/folders.html.twig', [
            'currentFolder' => $folder,
            'path' => $entityManager->getRepository(Folder::class)->getPath($folder),
            'folderTrash' => $folderTrash,
            'folderRoot' => $folderRoot,
        ]);
    }

    private function setupNewForest(EntityManagerInterface $entityManager): void
    {
        $everyOneGroup = $entityManager->getRepository(Group::class)->findOneBy(['name'=>'Everyone']);
        if (null == $everyOneGroup) {
            $everyOneGroup = new Group();
            $everyOneGroup->setName('Everyone');
            $entityManager->persist($everyOneGroup);
            $entityManager->flush();
        }
        $authenticatedUsersGroup = $entityManager->getRepository(Group::class)->findOneBy(['name'=>'Authenticated users']);
        if (null == $authenticatedUsersGroup) {
            $authenticatedUsersGroup = new Group();
            $authenticatedUsersGroup->setName('Authenticated users');
            $entityManager->persist($authenticatedUsersGroup);
            $entityManager->flush();
        }
        $administratorsGroup = $entityManager->getRepository(Group::class)->findOneBy(['name'=>'Administrators']);
        if (null == $administratorsGroup) {
            $administratorsGroup = new Group();
            $administratorsGroup->setName('Administrators');
            $entityManager->persist($administratorsGroup);
            $entityManager->flush();
        }
        $folder = $entityManager->getRepository(Folder::class)->findOneBy(['title'=>'*** ROOT ***']);
        if (null == $folder) {
            $folder = new Folder();
            $folder->setTitle('*** ROOT ***');
            $entityManager->persist($folder);
            $acl1 = new GroupAcl();
            $acl1->setGroup($everyOneGroup);
            $acl1->setPermission('view');
            $entityManager->persist($acl1);
            $entityManager->flush();
            $acl2 = new GroupAcl();
            $acl2->setGroup($administratorsGroup);
            $acl2->setPermission('edit');
            $entityManager->persist($acl2);
            $entityManager->flush();
            $security = new Security();
            $security->addGroupAcl($acl1);
            $security->addGroupAcl($acl2);
            $entityManager->persist($security);
            $entityManager->flush();
            $folder->setSecurity($security);
            $acl1->setSecurity($security);
            $acl2->setSecurity($security);
            $entityManager->flush();
        }
        $folder = $entityManager->getRepository(Folder::class)->findOneBy(['title'=>'*** TRASH ***']);
        if (null == $folder) {
            $folder = new Folder();
            $folder->setTitle('*** TRASH ***');
            $entityManager->persist($folder);
            $acl2 = new GroupAcl();
            $acl2->setGroup($administratorsGroup);
            $acl2->setPermission('edit');
            $entityManager->persist($acl2);
            $entityManager->flush();
            $security = new Security();
            $security->addGroupAcl($acl2);
            $entityManager->persist($security);
            $entityManager->flush();
            $folder->setSecurity($security);
            $acl2->setSecurity($security);
            $entityManager->flush();
        }
    }

    #[Route('/test', name: 'test')]
    public function test(): Response
    {
        $env = $this->getParameter('kernel.environment');

        return new Response($env);
    }
}
