<?php

namespace App\Controller;

use App\Entity\Document;
use App\Entity\Folder;
use App\Entity\Link;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Attributes as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/info')]
class InfoApiController extends AbstractController
{
    #[Route(path: '/{itemId}', name: 'api-info-item', methods: 'GET')]
    #[OA\Response(response: 200, description: 'Item info')]
    public function info(EntityManagerInterface $entityManager, string $itemId): JsonResponse
    {
        try {
            $item = $entityManager->find(Document::class, $itemId);
            if ($item) {
                $this->denyAccessUnlessGranted('view', $item);

                return new JsonResponse(
                    [
                        'id' => $item->getId(),
                        'title' => $item->getTitle(),
                        'description' => $item->getDescription(),
                        'type' => 'document',
                        'extension' => $item->getMediaFile()->getExtension(),
                        'size' => $item->getMediaFile()->getSize(),
                        'url' => $this->generateUrl('api-download-document', ['documentId' => $item->getId()]),
                        'parentFolderId' => $item->getFolder() ? $item->getFolder()->getId() : '',
                        'slug' => $item->getSlug(),
                        'createdAt' => $item->getCreatedAt()->format('Y-m-d h:i:s'),
                        'updatedAt' => $item->getUpdatedAt()->format('Y-m-d h:i:s'),
                        'fileUpdatedAt' => $item->getFileUpdatedAt()->format('Y-m-d h:i:s'),
                        'tags' => $item->getTags(),
                    ]
                );
            }
            $item = $entityManager->find(Folder::class, $itemId);
            if ($item) {
                $this->denyAccessUnlessGranted('view', $item);

                return new JsonResponse(
                    [
                        'id' => $item->getId(),
                        'title' => $item->getTitle(),
                        'description' => $item->getDescription(),
                        'type' => 'folder',
                        'parentFolderId' => $item->getParent() ? $item->getParent()->getId() : '',
                        'slug' => $item->getSlug(),
                        'createdAt' => $item->getCreatedAt()->format('Y-m-d h:i:s'),
                        'updatedAt' => $item->getUpdatedAt()->format('Y-m-d h:i:s'),
                        'tags' => $item->getTags(),
                    ]
                );
            }
            $item = $entityManager->find(Link::class, $itemId);
            if ($item) {
                $this->denyAccessUnlessGranted('view', $item);

                return new JsonResponse(
                    [
                        'id' => $item->getId(),
                        'title' => $item->getTitle(),
                        'description' => $item->getDescription(),
                        'type' => 'link',
                        'url' => $this->generateUrl('api-goto-link', ['linkId' => $item->getId()]),
                        'parentFolderId' => $item->getFolder() ? $item->getFolder()->getId() : '',
                        'slug' => $item->getSlug(),
                        'createdAt' => $item->getCreatedAt()->format('Y-m-d h:i:s'),
                        'updatedAt' => $item->getUpdatedAt()->format('Y-m-d h:i:s'),
                        'tags' => $item->getTags(),
                    ]
                );
            }

            return new JsonResponse('not-found');
        } catch (ConversionException $e) {
            return new JsonResponse('not-valid-id-format');
        }
    }

    #[Route(path: '/get-folder/{folderId}', name: 'api-get-folder-full-info', methods: 'GET')]
    //    #[OA\Response(response: 200, description: 'Returns folder details including id, title, description, parent node id (if exists) and children nodes (if exist).', new OA\JsonContent(type: 'array', new OA\Items(new OA\Property(property: 'id', type: 'string', example: 'cde4d22e-e049-47a1-bd75-ee30de3d6ab5'), new OA\Property(property: 'title', type: 'string', example: 'Psicologia'), new OA\Property(property: 'description', type: 'string', example: null), new OA\Property(property: 'parentId', type: 'string', example: 'cde4d22e-1234-47a1-abcd-ee30de3d6ab5'), new OA\Property(property: 'children', type: 'array', new OA\Items(new OA\Property(property: 'id', type: 'string', example: 'cde4d22e-e049-47a1-bd75-ee30de3d6ab5'), new OA\Property(property: 'type', type: 'string', example: 'folder'), new OA\Property(property: 'title', type: 'string', example: 'Generale'), new OA\Property(property: 'description', type: 'string', example: null))))))]
    public function getFolder(string $folderId, EntityManagerInterface $entityManager): JsonResponse
    {
        if (null == $folderId) {
            // @phpstan-ignore-next-line
            $folder = $entityManager->getRepository(Folder::class)->findOneByTitle('*** ROOT ***');
        }
        try {
            $folder = $entityManager->getRepository(Folder::class)->find($folderId);
        } catch (ConversionException $e) {
            $folder = $entityManager->getRepository(Folder::class)->findOneBy(['slug' => $folderId]);
        }

        $childrenNodes = [];
        foreach ($folder->getChildren() as $child) {
            $childrenNodes[$child->getTitle() . $child->getId()] = [
                'id' => $child->getId(),
                'title' => $child->getTitle(),
                'type' => 'folder',
                'description' => $child->getDescription(),
            ];
        }

        $documents = $entityManager->getRepository(Document::class)->findBy(['folder' => $folder]);

        foreach ($documents as $item) {
            $childrenNodes[$item->getTitle() . $item->getId()] = [
                'id' => $item->getId(),
                'title' => $item->getTitle(),
                'description' => $item->getDescription(),
                'type' => 'document',
                'extension' => $item->getMediaFile()->getExtension(),
                'size' => $item->getMediaFile()->getSize(),
                'url' => $this->generateUrl('api-download-document', ['documentId' => $item->getId()]),
                'parentFolderId' => $item->getFolder() ? $item->getFolder()->getId() : '',
                'slug' => $item->getSlug(),
                'createdAt' => $item->getCreatedAt()->format('Y-m-d h:i:s'),
                'updatedetAt' => $item->getCreatedAt()->format('Y-m-d h:i:s'),
                'fileUpdatedAt' => $item->getFileUpdatedAt()->format('Y-m-d h:i:s'),
                'tags' => $item->getTags(),
            ];
        }

        $links = $entityManager->getRepository(Link::class)->findBy(['folder' => $folder]);

        foreach ($links as $item) {
            $childrenNodes[$item->getTitle() . $item->getId()] = [
                'id' => $item->getId(),
                'title' => $item->getTitle(),
                'description' => $item->getDescription(),
                'type' => 'link',
                'url' => $this->generateUrl('api-goto-link', ['linkId' => $item->getId()]),
                'parentFolderId' => $item->getFolder() ? $item->getFolder()->getId() : '',
                'slug' => $item->getSlug(),
                'createdAt' => $item->getCreatedAt()->format('Y-m-d h:i:s'),
                'updatedetAt' => $item->getCreatedAt()->format('Y-m-d h:i:s'),
                'tags' => $item->getTags(),
            ];
        }
        ksort($childrenNodes);

        $node = [
            'id' => $folder->getId(),
            'title' => $folder->getTitle(),
            'description' => $folder->getDescription(),
            'parentId' => ($folder->getParent()) ? $folder->getParent()->getId() : null,
            'children' => $childrenNodes,
        ];

        return new JsonResponse($node);
    }


    #[Route(path: '/tag/{tag}', name: 'api-info-tag', methods: 'GET')]
    //    #[OA\Parameter(name: 'q', in: 'query', required: false, description: 'Terms for search', new OA\Schema(type: 'string'))]
        //    #[OA\Response(response: 200, description: 'Return search results including id, title, description, type of item and score. Results are ordered descending by score.', new OA\JsonContent(type: 'array', new OA\Items(new OA\Property(property: 'score', type: 'float', example: '24.87'), new OA\Property(property: 'id', type: 'string', example: 'cde4d22e-e049-47a1-bd75-ee30de3d6ab5'), new OA\Property(property: 'title', type: 'string', example: 'Tesi 2023'), new OA\Property(property: 'description', type: 'string', example: 'this is a node'), new OA\Property(property: 'type', type: 'string', example: 'folder'))))]
    public function search(string $tag, EntityManagerInterface $entityManager): JsonResponse
    {

        if ($tag == '*') {
            $documents = $entityManager->getRepository(Document::class)->findAll();
        } else {
            $documents = $entityManager->getRepository(Document::class)->findByTag($tag);
            $folders = $entityManager->getRepository(Folder::class)->createQueryBuilder('f')
                ->join('f.tags', 't')
                ->andWhere('lower(t.title) = :val')
                ->setParameter('val', strtolower($tag))
                ->getQuery()
                ->getResult();
            foreach ($folders as $folder) {
                $myFolders = $this->getAllChildern($folder);
                foreach ($myFolders as $myFolder) {
                    $documents2 = $entityManager->getRepository(Document::class)->findByFolder($myFolder);
                    foreach ($documents2 as $document) {
                        $documents[] = $document;
                    }
                }
            }


        }


        $list = [];
        foreach ($documents as $document) {
            if ($this->isGranted('view', $document)) {
                $list[] = [
                    'id' => $document->getId(),
                    'title' => $document->getTitle(),
                    'description' => $document->getDescription(),
                    'type' => 'document',
                    'extension' => $document->getMediaFile()->getExtension(),
                    'size' => $document->getMediaFile()->getSize(),
                    'url' => $this->generateUrl('api-download-document', ['documentId' => $document->getId()]),
                    'parentFolderId' => $document->getFolder() ? $document->getFolder()->getId() : null,
                    'slug' => $document->getSlug(),
                    'createdAt' => $document->getCreatedAt()->format('Y-m-d h:i:s'),
                    'updatedetAt' => $document->getCreatedAt()->format('Y-m-d h:i:s'),
                ];
            }
        }

        return new JsonResponse($list);
    }


    /**
     * @return Folder[]
     */
    private function getAllChildern(Folder $folder)
    {

        if (count($folder->getChildren())>0) {
            $list = [];
            $list[] = $folder;
            foreach ($folder->getChildren() as $folder) {
                $list[] = $folder;
                $childer = $this->getAllChildern($folder);
                foreach ($childer as $child) {
                    $list[] = $child;
                }
            }
            return $list;
        } else {
            return [$folder];
        }
    }
}
