<?php

namespace App\Controller;

use App\Entity\Document;
use App\Entity\Link;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Attributes as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Handler\DownloadHandler;

#[Route('/api/document')]
class DocumentApiController extends AbstractController
{
    #[Route('/download/{documentId}', name: 'api-download-document', methods: 'GET')]
    #[OA\Response(
        response: 200,
        description: 'Returns document stream',
    )]
    public function download(EntityManagerInterface $entityManager, string $documentId, DownloadHandler $downloadHandler): Response
    {
        $document = $entityManager->find(Document::class, trim($documentId));
        if ($document && $document->getMediaFile()) {
            $fileName = $document->getTitle() . '.' . $document->getMediaFile()->getExtension();

            return $downloadHandler->downloadObject($document, 'mediaFile', null, $fileName);
        }

        return new JsonResponse('not-found');
    }

    #[Route('/goto/{linkId}', name: 'api-goto-link', methods: 'GET')]
    #[OA\Response(response: 200, description: 'Goto to link URL')]
    public function goto(EntityManagerInterface $entityManager, string $linkId): Response
    {
        $linkId = trim($linkId);

        if (strlen($linkId) > 36) {
            $linkId = substr($linkId, 0, 36);
        }
        $link = $entityManager->find(Link::class, $linkId);

        return $this->redirect($link->getUrl());
    }
}
