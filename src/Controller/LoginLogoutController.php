<?php

namespace App\Controller;

use App\Form\UserLoginType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginLogoutController extends AbstractController
{
    #[Route(path: '/local/login', name: 'login-local', methods: ['GET', 'POST'])]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        $form = $this->createForm(UserLoginType::class);

        return $this->render(
            'login_logout/login_local.html.twig',
            [
                'form' => $form,
                'last_username' => $lastUsername,
                'error' => $error,
            ]
        );
    }

    #[Route(path: '/logout', name: 'logout', methods: ['GET'])]
    public function logout(TokenStorageInterface $csrfTokenStorage): Response
    {
        $csrfTokenStorage->setToken(null);

        return $this->redirectToRoute('cas_bundle_logout');
    }
}
