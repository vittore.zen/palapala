<?php

namespace App\Controller;

use App\Entity\Folder;
use App\Entity\Link;
use App\Form\LinkType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/link')]
class LinkController extends AbstractController
{
    #[Route('/new/{folderId}', name: 'app_link_new', methods: ['GET', 'POST'])]
    public function new(string $folderId, Request $request, EntityManagerInterface $entityManager): Response
    {
        $link = new Link();
        $folder = $entityManager->getRepository(Folder::class)->find($folderId);
        $link->setFolder($folder);
        $form = $this->createForm(LinkType::class, $link);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($link);
            $entityManager->flush();
            return $this->redirectToRoute('folders', ['folderId' => $folder->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('link/new.html.twig', [
            'link' => $link,
            'form' => $form,
            'parentId' => $folderId,
        ]);
    }

    #[Route('/{linkId}/goto', name: 'app_link_goto', methods: ['GET'])]
    public function goto(EntityManagerInterface $entityManager, string $linkId): Response
    {
        $link = $entityManager->find(Link::class, trim($linkId));

        return $this->redirect($link->getUrl());
    }

    #[Route('/{linkId}/edit', name: 'app_link_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, string $linkId, EntityManagerInterface $entityManager): Response
    {
        $link = $entityManager->find(Link::class, $linkId);
        $form = $this->createForm(LinkType::class, $link);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('folders', ['folderId' => $link->getFolder()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('link/edit.html.twig', [
            'link' => $link,
            'form' => $form,
        ]);
    }

    #[Route('/{linkId}/move', name: 'app_link_move', methods: ['GET', 'POST'])]
    public function move(Request $request, string $linkId, EntityManagerInterface $entityManager): Response
    {
        $link = $entityManager->find(Link::class, $linkId);
        $form = $this->createForm(LinkType::class, $link);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('folders', ['folderId' => $link->getFolder()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('link/edit.html.twig', [
            'link' => $link,
            'form' => $form,
        ]);
    }

    #[Route('/{linkId}/delete', name: 'app_link_delete', methods: ['GET'])]
    public function delete(Request $request, string $linkId, EntityManagerInterface $entityManager): Response
    {
        $link = $entityManager->find(Link::class, $linkId);
        $this->denyAccessUnlessGranted('edit', $link);
        $folderTrash = $entityManager->getRepository(Folder::class)->findOneBy(['title'=>'*** TRASH ***']);
        $folderId = $link->getFolder()->getId();
        $link->setFolder($folderTrash);

        $entityManager->flush();

        return $this->redirectToRoute('folders', ['folderId' => $folderId], Response::HTTP_SEE_OTHER);
    }
}
