<?php

namespace App\Controller;

use App\Entity\Folder;
use App\Service\Finder;
use OpenApi\Attributes as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/search')]
class SearchApiController extends AbstractController
{
    #[Route(path: '/', name: 'api-search', methods: 'GET')]
    //    #[OA\Parameter(name: 'q', in: 'query', required: false, description: 'Terms for search', new OA\Schema(type: 'string'))]
    //    #[OA\Response(response: 200, description: 'Return search results including id, title, description, type of item and score. Results are ordered descending by score.', new OA\JsonContent(type: 'array', new OA\Items(new OA\Property(property: 'score', type: 'float', example: '24.87'), new OA\Property(property: 'id', type: 'string', example: 'cde4d22e-e049-47a1-bd75-ee30de3d6ab5'), new OA\Property(property: 'title', type: 'string', example: 'Tesi 2023'), new OA\Property(property: 'description', type: 'string', example: 'this is a node'), new OA\Property(property: 'type', type: 'string', example: 'folder'))))]
    public function search(Finder $finder, Request $request): JsonResponse
    {
        $q = $request->get('q', '');
        $results = $finder->search($q);
        $list = [];
        foreach ($results as $result) {
            if ($this->isGranted('view', $result['object'])) {
                unset($result['object']);
                $list[] = $result;
            }
        }

        return new JsonResponse($list);
    }



}
