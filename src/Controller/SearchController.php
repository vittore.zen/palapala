<?php

namespace App\Controller;

use App\Service\Finder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    #[Route('/search', name: 'search', methods: ['GET'])]
    public function search(Finder $finder, Request $request): Response
    {
        $q = $request->get('q', '');
        $results = $finder->search($q);
        $list = [];
        foreach ($results as $result) {
            if ($this->isGranted('view', $result['object'])) {
                $list[] = $result;
            }
        }

        return $this->render('search/search.html.twig', [
            'results' => $list,
            'q' => $q,
        ]);
    }
}
