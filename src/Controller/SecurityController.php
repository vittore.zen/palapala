<?php

namespace App\Controller;

use App\Entity\Document;
use App\Entity\Folder;
use App\Entity\Security;
use App\Form\SecurityType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/security')]
class SecurityController extends AbstractController
{
    #[Route('/edit/{type}/{id}', name: 'object_security_edit', methods: ['GET', 'POST'])]
    public function edit(string $type, string $id, Request $request, EntityManagerInterface $entityManager): Response
    {
        $object = null;
        $folderId = null;
        if ('folder' == $type) {
            $object = $entityManager->find(Folder::class, $id);
            $folderId = $object->getId();
        }
        if ('document' == $type) {
            $object = $entityManager->find(Document::class, $id);
            $folderId = $object->getFolder()->getId();
        }
        if (!$object) {
            throw new NotFoundHttpException('Invalid object');
        }
        if (!$this->isGranted('edit', $object)) {
            throw new \Exception('No access');
        }
        $security = $object->getSecurity();
        if (!$security) {
            $security = new Security();
            $entityManager->persist($security);
            $object->setSecurity($security);
            $entityManager->flush();
        }

        $form = $this->createForm(SecurityType::class, $security, [
            'action' => $this->generateUrl('object_security_edit', ['type' => $type, 'id' => $id]),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('folders', ['folderId' => $folderId], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('security/edit.html.twig', [
            'object' => $object,
            'form' => $form,
        ]);
    }
}
