<?php

namespace App\Controller;

use App\Entity\Document;
use App\Entity\Folder;
use App\Form\DocumentMoveType;
use App\Form\DocumentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Handler\DownloadHandler;

#[Route('/document')]
class DocumentController extends AbstractController
{
    #[Route('/new/{folderId}', name: 'app_document_new', methods: ['GET', 'POST'])]
    public function new(string $folderId, Request $request, EntityManagerInterface $entityManager): Response
    {
        $document = new Document();
        $folder = $entityManager->getRepository(Folder::class)->find($folderId);
        $document->setFolder($folder);
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getData()->getMediaFile()) {
                $document->setFileUpdatedAt(new \DateTime());
            }
            $entityManager->persist($document);
            $entityManager->flush();
            return $this->redirectToRoute('folders', ['folderId' => $folder->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('document/new.html.twig', [
            'document' => $document,
            'form' => $form,
            'parentId' => $folderId,
        ]);
    }

    #[Route('/{documentId}/download', name: 'app_document_download', methods: ['GET'])]
    public function download(EntityManagerInterface $entityManager, string $documentId, DownloadHandler $downloadHandler, bool $no_index): Response
    {
        $document = $entityManager->find(Document::class, trim($documentId));
        if ($document->getMediaFile()) {
            $fileName = $document->getTitle() . '.' . $document->getMediaFile()->getExtension();
            $response = $downloadHandler->downloadObject($document, 'mediaFile', null, $fileName);
            if ($no_index) {
                $response->headers->set('X-Robots-Tag', 'noindex');
            }
        } else {
            $response = new Response('No-file');
        }

        return $response;
    }

    #[Route('/{documentId}/show', name: 'app_document_show', methods: ['GET'])]
    public function show(EntityManagerInterface $entityManager, string $documentId, DownloadHandler $downloadHandler, bool $no_index): Response
    {
        $document = $entityManager->find(Document::class, trim($documentId));
        if ($document->getMediaFile()) {
            $response = $downloadHandler->downloadObject($document, 'mediaFile', null, null, false);
            if ($no_index) {
                $response->headers->set('X-Robots-Tag', 'noindex');
            }
        } else {
            $response = new Response('No-file');
        }

        return $response;
    }

    #[Route('/{documentId}/get-dates', name: 'app_document_get_dates', methods: ['GET'])]
    public function getDates(EntityManagerInterface $entityManager, string $documentId, DownloadHandler $downloadHandler): JsonResponse
    {
        $document = $entityManager->find(Document::class, $documentId);

        return new JsonResponse([
            'created' => $document->getCreatedAt()->format('Y-m-d H:i'),
            'updated' => $document->getUpdatedAt()->format('Y-m-d H:i'),
            'fileUpdated' => $document->getFileUpdatedAt()->format('Y-m-d H:i'),
        ]);
    }

    #[Route('/{documentId}/edit', name: 'app_document_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, string $documentId, EntityManagerInterface $entityManager): Response
    {
        $document = $entityManager->find(Document::class, trim($documentId));
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getData()->getMediaFile()) {
                $document->setFileUpdatedAt(new \DateTime());
            }
            $entityManager->flush();

            return $this->redirectToRoute('folders', ['folderId' => $document->getFolder()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('document/edit.html.twig', [
            'document' => $document,
            'form' => $form,
        ]);
    }

    #[Route('/{documentId}/move', name: 'app_document_move', methods: ['GET', 'POST'])]
    public function move(Request $request, string $documentId, EntityManagerInterface $entityManager): Response
    {
        $document = $entityManager->find(Document::class, trim($documentId));
        $form = $this->createForm(DocumentMoveType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('folders', ['folderId' => $document->getFolder()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('document/move.html.twig', [
            'document' => $document,
            'form' => $form,
        ]);
    }

    #[Route('/{documentId}/delete', name: 'app_document_delete', methods: ['GET'])]
    public function delete(Request $request, string $documentId, EntityManagerInterface $entityManager): Response
    {
        $document = $entityManager->find(Document::class, trim($documentId));
        $this->denyAccessUnlessGranted('edit', $document);
        $folderTrash = $entityManager->getRepository(Folder::class)->findOneBy(['title' => '*** TRASH ***']);
        $folderId = $document->getFolder()->getId();
        $document->setFolder($folderTrash);

        $entityManager->flush();

        return $this->redirectToRoute('folders', ['folderId' => $folderId], Response::HTTP_SEE_OTHER);
    }
}
