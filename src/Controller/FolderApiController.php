<?php

namespace App\Controller;

use App\Entity\Folder;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Attributes as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/folder')]
class FolderApiController extends AbstractController
{
    #[Route(path: '/get-root-folder', name: 'api-get-root-folder', methods: 'GET')]
    /*
        #[OA\Response(
            response: 200,
            description: 'Returns root folder details including id, title, description and children nodes (if exist).',
            content: new OA\JsonContent(
                type: 'array',
                items: new OA\Items(
                    new OA\Property(property: 'id', type: 'string', example: 'cde4d22e-e049-47a1-bd75-ee30de3d6ab5'),
                    new OA\Property(property: 'title', type: 'string', example: '*** ROOT ***'),
                    new OA\Property(property: 'description', type: 'string', example: 'this is a node'),
                    new OA\Property(property: 'parentId', type: 'string', example: null) /*,
                    new OA\Property(property: 'children', type: 'array',
                        items: new OA\Items(
                            new OA\Property(property: 'id', type: 'string', example: 'cde4d22e-e049-47a1-bd75-ee30de3d6ab5'),
                            new OA\Property(property: 'title', type: 'string', example: 'Generale'),
                            new OA\Property(property: 'description', type: 'string', example: ''
                            )
                        )
                    )
                )
            )
        )]
    */
    public function getFolderRoot(EntityManagerInterface $entityManager): JsonResponse
    {
        $folder = $entityManager->getRepository(Folder::class)->findOneBy(['title'=>'*** ROOT ***']);

        $childrenNodes = [];
        foreach ($folder->getChildren() as $child) {
            $childrenNodes[] = [
                'id' => $child->getId(),
                'title' => $child->getTitle(),
                'description' => $child->getDescription(),
            ];
        }

        $node = [
            'id' => $folder->getId(),
            'title' => $folder->getTitle(),
            'description' => $folder->getDescription(),
            'parentId' => ($folder->getParent()) ? $folder->getParent()->getId() : null,
            'children' => $childrenNodes,
        ];

        return new JsonResponse($node);
    }

    #[Route(path: '/get-folder/{folderId}', name: 'api-get-folder', methods: 'GET')]
    //    #[OA\Response(response: 200, description: 'Returns folder details including id, title, description, parent node id (if exists) and children nodes (if exist).', content: new OA\JsonContent(type: 'array', items: new OA\Items(new OA\Property(property: 'id', type: 'string', example: 'cde4d22e-e049-47a1-bd75-ee30de3d6ab5'), new OA\Property(property: 'title', type: 'string', example: 'Psicologia'), new OA\Property(property: 'description', type: 'string', example: null), new OA\Property(property: 'parentId', type: 'string', example: 'cde4d22e-1234-47a1-abcd-ee30de3d6ab5'), new OA\Property(property: 'children', type: 'array', items: new OA\Items(new OA\Property(property: 'id', type: 'string', example: 'cde4d22e-e049-47a1-bd75-ee30de3d6ab5'), new OA\Property(property: 'title', type: 'string', example: 'Generale'), new OA\Property(property: 'description', type: 'string', example: null))))))]
    public function getFolder(string $folderId, EntityManagerInterface $entityManager): JsonResponse
    {
        if (null == $folderId) {
            $folder = $entityManager->getRepository(Folder::class)->findOneBy(['title'=>'*** ROOT ***']);
        }
        try {
            $folder = $entityManager->getRepository(Folder::class)->find($folderId);
        } catch (ConversionException $e) {
            $folder = $entityManager->getRepository(Folder::class)->findOneBy(['slug'=>$folderId]);
        }

        $childrenNodes = [];
        foreach ($folder->getChildren() as $child) {
            $childrenNodes[] = [
                'id' => $child->getId(),
                'title' => $child->getTitle(),
                'description' => $child->getDescription(),
            ];
        }

        $node = [
            'id' => $folder->getId(),
            'title' => $folder->getTitle(),
            'description' => $folder->getDescription(),
            'parentId' => ($folder->getParent()) ? $folder->getParent()->getId() : null,
            'children' => $childrenNodes,
        ];

        return new JsonResponse($node);
    }

    #[Route(path: '/get-path/{folderId}', name: 'api-get-path', methods: 'GET')]
    //    #[OA\Response(response: 200, description: 'Returns path of nodes.', content: new OA\JsonContent(type: 'array', items: new OA\Items(new OA\Property(property: 'id', type: 'string', example: 'cde4d22e-e049-47a1-bd75-ee30de3d6ab5'), new OA\Property(property: 'title', type: 'string', example: 'Psicologia'), new OA\Property(property: 'description', type: 'string', example: null))))]
    public function getPath(string $folderId, EntityManagerInterface $entityManager): JsonResponse
    {
        try {
            $folder = $entityManager->getRepository(Folder::class)->find($folderId);
        } catch (ConversionException $e) {
            $folder = $entityManager->getRepository(Folder::class)->findOneBy(['slug'=>$folderId]);
        }

        $pathNodes = [];
        $breadcumbs = $entityManager->getRepository(Folder::class)->getPath($folder);
        foreach ($breadcumbs as $node) {
            $pathNodes[] = [
                'id' => $node->getId(),
                'title' => $node->getTitle(),
                'description' => $node->getDescription(),
            ];
        }

        return new JsonResponse($pathNodes);
    }

    #[Route(path: '/ping', name: 'api-echo', methods: 'GET')]
    #[OA\Response(response: 200, description: 'Returns a test string.',
        content: new OA\JsonContent(
            type: 'string',
            example: 'pong'
        )
    )]
    public function echo(Request $request): JsonResponse
    {
        return new JsonResponse('pong');
    }
}
