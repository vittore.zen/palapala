<?php

namespace App\Controller;

use App\Entity\Folder;
use App\Form\FolderMoveType;
use App\Form\FolderType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/folder-actions')]
class FolderController extends AbstractController
{
    #[Route('/{parentId}/new', name: 'app_folder_new', methods: ['GET', 'POST'])]
    public function new(string $parentId, Request $request, EntityManagerInterface $entityManager): Response
    {
        $parentFolder = $entityManager->find(Folder::class, $parentId);
        $this->denyAccessUnlessGranted('edit', $parentFolder);
        $folder = new Folder();
        $form = $this->createForm(FolderType::class, $folder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($folder);
            $folder->setParent($parentFolder);

            $entityManager->flush();

            return $this->redirectToRoute('folders', ['folderId' => $folder->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('folder/new.html.twig', [
            'folder' => $folder,
            'parentId' => $parentId,
            'form' => $form,
        ]);
    }

    #[Route('/{folderId}/edit', name: 'app_folder_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, string $folderId, EntityManagerInterface $entityManager): Response
    {
        $folder = $entityManager->find(Folder::class, $folderId);
        $this->denyAccessUnlessGranted('edit', $folder);
        $form = $this->createForm(FolderType::class, $folder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('folders', ['folderId' => $folder->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('folder/edit.html.twig', [
            'folder' => $folder,
            'form' => $form,
        ]);
    }

    #[Route('/{folderId}/move', name: 'app_folder_move', methods: ['GET', 'POST'])]
    public function move(Request $request, string $folderId, EntityManagerInterface $entityManager): Response
    {
        $folder = $entityManager->find(Folder::class, $folderId);
        $this->denyAccessUnlessGranted('edit', $folder);
        $form = $this->createForm(FolderMoveType::class, $folder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('folders', ['folderId' => $folder->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('folder/move.html.twig', [
            'folder' => $folder,
            'form' => $form,
        ]);
    }

    #[Route('/{folderId}/delete', name: 'app_folder_delete', methods: ['GET'])]
    public function delete(Request $request, string $folderId, EntityManagerInterface $entityManager): Response
    {
        $folder = $entityManager->find(Folder::class, $folderId);
        $this->denyAccessUnlessGranted('edit', $folder);
        $oldParentId = $folder->getParent()->getId();
        $folderTrash = $entityManager->getRepository(Folder::class)->findOneBy(['title'=>'*** TRASH ***']);
        $folder->setParent($folderTrash);

        $entityManager->flush();

        return $this->redirectToRoute('folders', ['folderId' => $oldParentId], Response::HTTP_SEE_OTHER);
    }
}
