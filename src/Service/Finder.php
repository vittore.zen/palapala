<?php

namespace App\Service;

use App\Entity\Folder;
use Doctrine\ORM\EntityManagerInterface;
use FOS\ElasticaBundle\Finder\FinderInterface;

class Finder
{
    /** @var FinderInterface */
    private $finderFolder;

    /** @var FinderInterface */
    private $finderDocument;

    /**
     * @var FinderInterface
     */
    private $finderLink;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, FinderInterface $finderFolder, FinderInterface $finderDocument, FinderInterface $finderLink)
    {
        $this->finderFolder = $finderFolder;
        $this->finderDocument = $finderDocument;
        $this->finderLink = $finderLink;
        $this->entityManager = $entityManager;
    }

    public function search(string $query): mixed
    {
        $query = str_replace('/', '//', $query);
        $resultsFolder = $this->finderFolder->findHybrid($query);
        $resultsDocument = $this->finderDocument->findHybrid($query);
        $resultsLink = $this->finderLink->findHybrid($query);
        $finalResults = [];
        foreach ($resultsFolder as $item) {
            $finalResults[] = [
                'score' => $item->getResult()->getScore(),
                'id' => (string) $item->getTransformed()->getId(),
                'title' => $item->getTransformed()->getTitle(),
                'description' => $item->getTransformed()->getDescription(),
                'type' => 'folder',
                'object' => $item->getTransformed(),
                'path' => $this->getPathNodes($item->getTransformed()->getParent()),
            ];
        }
        foreach ($resultsDocument as $item) {
            $finalResults[] = [
                'score' => $item->getResult()->getScore(),
                'id' => (string) $item->getTransformed()->getId(),
                'title' => $item->getTransformed()->getTitle(),
                'description' => $item->getTransformed()->getDescription(),
                'type' => 'document',
                'object' => $item->getTransformed(),
                'path' => $this->getPathNodes($item->getTransformed()->getFolder()),
            ];
        }
        foreach ($resultsLink as $item) {
            $finalResults[] = [
                'score' => $item->getResult()->getScore(),
                'id' => (string) $item->getTransformed()->getId(),
                'title' => $item->getTransformed()->getTitle(),
                'description' => $item->getTransformed()->getDescription(),
                'type' => 'link',
                'object' => $item->getTransformed(),
                'path' => $this->getPathNodes($item->getTransformed()->getFolder()),
            ];
        }
        usort($finalResults, function ($item1, $item2) {
            return -($item1['score'] <=> $item2['score']);
        });

        return $finalResults;
    }

    private function getPathNodes(?Folder $folder): mixed
    {
        if (is_null($folder)) {
            return [];
        }
        $pathNodes = [];
        $breadcumbs = $this->entityManager->getRepository(Folder::class)->getPath($folder);
        foreach ($breadcumbs as $node) {
            $pathNodes[] = [
                'id' => $node->getId(),
                'title' => $node->getTitle(),
                'description' => $node->getDescription(),
            ];
        }

        return $pathNodes;
    }
}
