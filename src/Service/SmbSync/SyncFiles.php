<?php

namespace App\Service\SmbSync;

use App\Entity\Document;
use App\Entity\Folder;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Finder\Finder;
use Vich\UploaderBundle\FileAbstraction\ReplacingFile;

class SyncFiles
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function sync($folders): void
    {
        $folderRoot = $this->entityManager->getRepository(Folder::class)->findOneBy(['title' => '*** ROOT ***']);
        foreach ($folders as $folderItem) {
            $folderName = $folderItem['name'];
            $folder = null;
            foreach ($folderRoot->getChildren() as $child) {
                if ($child->getTitle() == $folderName) {
                    $folder = $child;
                    break;
                }
            }
            if ($folder) {
                $this->syncFolder($folder, $folderItem['mounting-point']);
            }
        }
    }

    private function syncFolder(Folder $folder, string $mountingPoint): void
    {
        $currentFiles = [];
        $finder = new Finder();
        $finder->in($mountingPoint)->files();
        if ($finder->count() > 0) {
            echo "\n$mountingPoint";
            foreach ($finder as $file) {
                echo "\n" . $file->getRelativePath();
                $fileFolder = $this->getFolder($folder, $file->getRelativePath());
                $document = $this->copyFile($fileFolder, $file);
                $currentFiles[(string)$document->getId()] = $document;
            }
        }
        $allFiles = $this->getAllFiles($folder);
        $this->removeZombieFiles($allFiles, $currentFiles);

    }


    private function getFolder(Folder $folder, string $path): Folder
    {


        if ($path == '') {
            return $folder;
        }
        $steps = explode('/', $path);
        //      echo "\n folder: ".$folder->getTitle();
        foreach ($steps as $step) {
            //           echo "\n   - step: " . $step;
            $found = false;
            foreach ($folder->getChildren() as $child) {
                if ($child->getTitle() == $step) {
                    $found = true;
                    $folder = $child;
//                    echo " -> ".$child->getTitle();
                    break;
                }
            }
        }
        if ($found) {
            return $folder;
        }
        throw new \Exception('Parent folder ' . $folder->getTitle() . ', path ' . $path);

        return $folder;
    }


    private function copyFile(Folder $folder, \SplFileInfo $file): Document
    {
        echo("\n" . 'copy ' . $folder->getTitle() . ' <- ' . $file->getPathname());

        $found = false;
        foreach ($folder->getDocuments() as $document) {
            if ($document->getTitle() == $file->getFilename()) {
                $found = true;

                $md5_1 = md5_file($file->getRealPath());
                $md5_2 = ($document->getMediaFile() && $document->getMediaFile()->getRealPath()) ? md5_file($document->getMediaFile()->getRealPath()) : '';
                if ($md5_1 != $md5_2) {
                    echo("\n" . 'File diversi, ricarico ' . $document->getTitle());
                    $document->setMediaFile(new ReplacingFile($file->getRealPath()));
                    $document->setFileUpdatedAt(new \DateTime());
                }
                break;
            }
        }
        if (!$found) {
            echo("\n" . 'ADD File non presente, aggiungo ' . $file->getFilename() . ' in ' . $folder->getTitle());

            $document = new Document();
            $document->setFolder($folder);
            $document->setTitle($file->getFilename());
            $document->setMediaFile(new ReplacingFile($file->getRealPath()));
            $this->entityManager->persist($document);

        }
        $this->entityManager->flush();

        return $document;
    }


    private function getAllFiles(Folder $folder): array
    {
        $folders = $this->getAllFolders($folder);
        $allFiles = [];
        foreach ($folders as $folderItem) {
            foreach ($folderItem->getDocuments() as $document) {
                $allFiles[(string)$document->getId()] = $document;
            }
        }
        return $allFiles;
    }

    private function getAllFolders(Folder $folder): array
    {

        $allFolders[(string)$folder->getId()] = $folder;
        foreach ($folder->getChildren() as $child) {
            $allFolders = array_merge($allFolders, $this->getAllFolders($child));
        }
        return $allFolders;
    }

    private function removeZombieFiles(array $allFiles, array $currentDocuments): void
    {

        $diff = array_diff($allFiles, $currentDocuments);
        foreach ($diff as $file) {

            $this->entityManager->remove($file);

        }
        $this->entityManager->flush();
    }

}
