<?php

namespace App\Service\SmbSync;

use App\Entity\Folder;
use App\Entity\Group;
use App\Entity\GroupAcl;
use App\Entity\Security;
use App\Entity\User;
use App\Security\PermissionParser;
use Doctrine\ORM\EntityManagerInterface;

class   SyncFoldersPermissions
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private PermissionParser       $permissionParser
    )
    {
    }

    public function sync($folders): void
    {
        $foldersToRetain = [];
        foreach ($folders as $folder) {
            $this->syncPermission($folder);
            $foldersToRetain[] = $folder['name'];
        }
        $folderRoot = $this->entityManager->getRepository(Folder::class)->findOneBy(['title' => '*** ROOT ***']);
        foreach ($folderRoot->getChildren() as $child) {
            if (!in_array($child->getTitle(), $foldersToRetain)) {
                echo "\nREMOVE FOLDER " . $child->getTitle() . "\n";
                $this->entityManager->remove($child);
            }
        }
        $this->entityManager->flush();
    }

    private function syncPermission($newFolder): void
    {
        $folderName = $newFolder['name'];
        echo "\n$folderName";
        $folderRoot = $this->entityManager->getRepository(Folder::class)->findOneBy(['title' => '*** ROOT ***']);
        $found = false;
        $folder = null;
        foreach ($folderRoot->getChildren() as $child) {
            if ($child->getTitle() == $folderName) {
                $found = true;
                $folder = $child;
            }
        }
        if (!$found) {
            $folder = new Folder();
            $folder->setTitle($folderName);
            $folder->setParent($folderRoot);
            $this->entityManager->persist($folder);
            $this->entityManager->flush();
        }
        $security = $folder->getSecurity();
        $groupAclToRetain = [];
        foreach ($newFolder['reader-groups'] as $groupName) {
            echo "\n - $groupName";
            $group = $this->entityManager->getRepository(Group::class)->findOneBy(['name' => $groupName]);
            if (!$security) {
                $security = new Security();
                $this->entityManager->persist($security);
                $folder->setSecurity($security);
                $this->entityManager->flush();
            }
            $found = false;
            foreach ($security->getGroupAcls() as $groupAclItem) {
                if ($groupAclItem->getGroup() and $groupAclItem->getGroup()->getName() == $groupName) {
                    $found = true;
                    $groupAcl = $groupAclItem;
                }
            }

            if (!$found) {
                $groupAcl = new GroupAcl();
                $groupAcl->setSecurity($security);
                $groupAcl->setGroup($group);
                $groupAcl->setPermission('view');
                $this->entityManager->persist($groupAcl);
            } else {
                $security->addGroupAcl($groupAcl);
            }
            $groupAclToRetain[] = (string)$groupAcl->getId();


            $this->entityManager->flush();
        }

        $found = false;
        $everyoneGroup = $this->entityManager->getRepository(Group::class)->findOneBy(['name' => 'Everyone']);
        foreach ($security->getGroupAcls() as $groupAclItem) {
            if ($groupAclItem->getGroup() and $groupAclItem->getGroup()->getId() == $everyoneGroup->getId()) {
                $found = true;
                $groupAcl = $groupAclItem;
            }
        }

        if (!$found) {
            $groupAcl = new GroupAcl();
            $groupAcl->setSecurity($security);
            $groupAcl->setGroup($everyoneGroup);
            $groupAcl->setPermission('no-access');
            $this->entityManager->persist($groupAcl);
            $this->entityManager->flush();

        }
        $groupAclToRetain[] = (string)$groupAcl->getId();


        $this->entityManager->flush();
        foreach ($security->getGroupAcls() as $groupAclItem) {
            if (!in_array((string)$groupAclItem->getId(), $groupAclToRetain)) {
                $this->entityManager->remove($groupAclItem);
            }
        }
        $this->entityManager->flush();
    }
}
