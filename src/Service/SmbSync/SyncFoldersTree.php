<?php

namespace App\Service\SmbSync;

use App\Entity\Document;
use App\Entity\Folder;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Finder\Finder;
use Vich\UploaderBundle\FileAbstraction\ReplacingFile;

class SyncFoldersTree
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function sync($folders): void
    {
        $folderRoot = $this->entityManager->getRepository(Folder::class)->findOneBy(['title' => '*** ROOT ***']);
        foreach ($folders as $folderItem) {
            $folderName = $folderItem['name'];
            $folder = null;
            foreach ($folderRoot->getChildren() as $child) {
                if ($child->getTitle() == $folderName) {
                    $folder = $child;
                    break;
                }
            }
            if ($folder) {
                $this->syncFolder($folder, $folderItem['mounting-point']);
            }
        }
    }

    private function syncFolder(Folder $folder, string $mountingPoint): void
    {
        $currentFolders[(string)$folder->getId()] = $folder;
        $finder = new Finder();
        $finder->in($mountingPoint)->directories();
        if ($finder->count() > 0) {
            foreach ($finder as $directory) {
                $newFolder = $this->createFolder($folder, $directory);
                $currentFolders[(string)$newFolder->getId()] = $newFolder;
            }
        }
        $allFolders = $this->getAllFolders($folder);
        $this->removeZombieFolders($allFolders, $currentFolders);

    }

    private function createFolder(Folder $folder, $directory): Folder
    {
        //        dump($directory->getPathname());
        $parentFolder = $this->findRelativePathFolder($folder, $directory->getRelativePath());
        $newFolder = $this->createFolderIfNotExist($parentFolder, $directory->getFilename());
        return $newFolder;

    }

    private function findRelativePathFolder(Folder $folder, string $path): Folder
    {

        if ($path == '') {
            return $folder;
        }
        $steps = explode('/', $path);
        //      echo "\n folder: ".$folder->getTitle();
        foreach ($steps as $step) {
            //           echo "\n   - step: " . $step;
            $found = false;
            foreach ($folder->getChildren() as $child) {
                if ($child->getTitle() == $step) {
                    $found = true;
                    $folder = $child;
//                    echo " -> ".$child->getTitle();
                    break;
                }
            }
        }
        if ($found) {
            return $folder;
        }
        throw new \Exception('Parent folder ' . $folder->getTitle() . ', path ' . $path);
    }


    private function createFolderIfNotExist(Folder $folder, string $filename): Folder
    {
//        echo "\n createFolderIfNotExist $filename";
//        echo "\n parented folder: " . $folder->getTitle();
        foreach ($folder->getChildren() as $child) {
            if ($child->getTitle() == $filename) {
                //              echo '**** Exists ***';
//                dump($child->getParent()->getTitle());
                return $child;
            }
        }
        $newFolder = new Folder();
        $newFolder->setTitle($filename);
        $newFolder->setParent($folder);
        $this->entityManager->persist($newFolder);
        $this->entityManager->flush();
        return $newFolder;
    }


    private function getAllFolders(Folder $folder): array
    {

        $allFolders[(string)$folder->getId()] = $folder;
        foreach ($folder->getChildren() as $child) {
            $allFolders = array_merge($allFolders, $this->getAllFolders($child));
        }
        return $allFolders;
    }

    private function removeZombieFolders(array $allFolders, array $currentFolders): void
    {

//        dump(array_keys($currentFolders), array_keys($allFolders));
        $diff=array_diff( $allFolders,$currentFolders);
        foreach ($diff as $folder) {

                $this->entityManager->remove($folder);

        }
        $this->entityManager->flush();
    }

}
