<?php

namespace App\Service\SmbSync;

use App\Entity\Group;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class SyncGroups
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function sync($groups): void
    {
        foreach ($groups as $group) {
            $this->syncSingleGroup($group);
        }
    }

    private function syncSingleGroup($newGroup): void
    {

        $group = $this->entityManager->getRepository(Group::class)->findOneBy(['name' => $newGroup['name']]);
        if (!$group) {
            $group = new Group();
            $group->setName($newGroup['name']);
            $this->entityManager->persist($group);
            $this->entityManager->flush();
        }
        foreach ($group->getUsers() as $user) {
            $group->removeUser($user);
        };

        if (isset($newGroup['users'])) {
            foreach ($newGroup['users'] as $newUser) {
                $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $newUser]);
                if (!$user) {
                    $user = new User();
                    $user->setUsername($newUser);
                    $user->setEmail($newUser);
                    $user->setPassword($newUser);
                    $user->setRoles(['ROLE_USER']);
                    $user->setLocalAuthentication(false);
                    $this->entityManager->persist($user);
                }
                $group->addUser($user);
            }
        }
        $this->entityManager->flush();
    }
}
