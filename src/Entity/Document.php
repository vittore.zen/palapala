<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Uid\Uuid;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: "App\Repository\DocumentRepository")]
#[Gedmo\Loggable]
#[ORM\HasLifecycleCallbacks]
#[Vich\Uploadable]
class Document
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    protected Uuid $id;

    #[ORM\Column(type: Types::STRING, nullable: true)]
    private ?string $title;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = '';

    #[ORM\Column(length: 128, unique: true, nullable: true)]
    #[Gedmo\Slug(fields: ['title'])]
    private ?string $slug;

    /**
     * @var File|null
     */
    #[Vich\UploadableField(mapping: 'mediaFiles', fileNameProperty: 'mediaName', size: 'mediaSize')]
    private $mediaFile;

    #[ORM\Column(type: Types::STRING, nullable: true)]
    private ?string $mediaName = '';

    #[ORM\Column(type: Types::INTEGER, nullable: true)]
    private ?int $mediaSize = 0;

    /**
     * @var Collection<int,Tag>
     */
    #[ORM\ManyToMany(targetEntity: "App\Entity\Tag", inversedBy: 'documents')]
    #[ORM\JoinTable(name: 'documents_tags')]
    private Collection $tags;

    /**
     * @var Folder|null
     */
    #[ORM\ManyToOne(targetEntity: 'Folder', inversedBy: 'documents')]
    #[ORM\JoinColumn(name: 'folder_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    private $folder;

    /**
     * One Document has One Security.
     */
    #[ORM\OneToOne(targetEntity: "App\Entity\Security")]
    #[ORM\JoinColumn(name: 'security_id', referencedColumnName: 'id')]
    private ?Security $security = null;

    #[Gedmo\Timestampable(on: 'create')]
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $createdAt = null;

    #[Gedmo\Timestampable(on: 'update')]
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $fileUpdatedAt = null;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->tags = new ArrayCollection();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = new Uuid($id);

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return trim($this->title);
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $mediaFile
     */
    public function setMediaFile(?File $mediaFile = null): void
    {
        $this->mediaFile = $mediaFile;

        if (null !== $mediaFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime();
        }
    }

    public function getMediaFile(): ?File
    {
        return $this->mediaFile;
    }

    public function setMediaName(?string $mediaName): void
    {
        $this->mediaName = $mediaName;
    }

    public function getMediaName(): ?string
    {
        return $this->mediaName;
    }

    public function setMediaSize(?int $mediaSize): void
    {
        $this->mediaSize = $mediaSize;
    }

    public function getMediaSize(): ?int
    {
        return $this->mediaSize;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): static
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
        }

        return $this;
    }

    public function removeTag(Tag $tag): static
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    public function getFolder(): ?Folder
    {
        return $this->folder;
    }

    public function setFolder(?Folder $folder): static
    {
        $this->folder = $folder;

        return $this;
    }

    public function getSecurity(): ?Security
    {
        return $this->security;
    }

    public function setSecurity(?Security $security): static
    {
        $this->security = $security;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getFileUpdatedAt(): ?\DateTimeInterface
    {
        return ($this->fileUpdatedAt) ? ($this->fileUpdatedAt) : ($this->updatedAt);
    }

    public function setFileUpdatedAt(?\DateTimeInterface $fileUpdatedAt): static
    {
        $this->fileUpdatedAt = $fileUpdatedAt;

        return $this;
    }
}
