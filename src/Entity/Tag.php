<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: "App\Repository\TagRepository")]
#[Gedmo\Loggable]
class Tag
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    protected Uuid $id;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, nullable: true)]
    private ?string $title;

    /**
     * @var Collection<int,Document>
     */
    #[ORM\ManyToMany(targetEntity: "App\Entity\Document", mappedBy: 'tags')]
    private Collection $documents;

    /**
     * @var Collection<int,Link>
     */
    #[ORM\ManyToMany(targetEntity: "App\Entity\Link", mappedBy: 'tags')]
    private Collection $links;

    /**
     * @var Collection<int,Folder>
     */
    #[ORM\ManyToMany(targetEntity: "App\Entity\Folder", mappedBy: 'tags')]
    private Collection $folders;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->documents = new ArrayCollection();
        $this->links = new ArrayCollection();
        $this->folders = new ArrayCollection();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = new Uuid($id);

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return trim($this->title);
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection<int, Document>
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents->add($document);
            $document->addTag($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            $document->removeTag($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Link>
     */
    public function getLinks(): Collection
    {
        return $this->links;
    }

    public function addLink(Link $link): static
    {
        if (!$this->links->contains($link)) {
            $this->links->add($link);
            $link->addTag($this);
        }

        return $this;
    }

    public function removeLink(Link $link): static
    {
        if ($this->links->removeElement($link)) {
            $link->removeTag($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Folder>
     */
    public function getFolders(): Collection
    {
        return $this->folders;
    }

    public function addFolder(Folder $folder): static
    {
        if (!$this->folders->contains($folder)) {
            $this->folders->add($folder);
            $folder->addTag($this);
        }

        return $this;
    }

    public function removeFolder(Folder $folder): static
    {
        if ($this->folders->removeElement($folder)) {
            $folder->removeTag($this);
        }

        return $this;
    }
}
