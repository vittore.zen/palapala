<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity()]
#[ORM\HasLifecycleCallbacks]
class Security
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    protected Uuid $id;

    /**
     * @var Collection<int, UserAcl>
     */
    #[ORM\OneToMany(targetEntity: 'UserAcl', mappedBy: 'security',
        fetch: 'EXTRA_LAZY',
        orphanRemoval: true,
        cascade: ['persist']
    )]
    private Collection $userAcls;

    /**
     * @var Collection<int, GroupAcl>
     */
    #[ORM\OneToMany(targetEntity: 'GroupAcl', mappedBy: 'security',
        fetch: 'EXTRA_LAZY',
        orphanRemoval: true,
        cascade: ['persist']
    )]
    private Collection $groupAcls;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->userAcls = new ArrayCollection();
        $this->groupAcls = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    /**
     * @return string[]
     */
    public static function getPermissions(): array
    {
        return
            [
                'edit' => 'edit',
                'view' => 'view',
                'no-access' => 'no-access',
            ];
    }

    /**
     * @return Collection<int, UserAcl>
     */
    public function getUserAcls(): Collection
    {
        return $this->userAcls;
    }

    public function addUserAcl(UserAcl $userAcl): self
    {
        if (!$this->userAcls->contains($userAcl)) {
            $this->userAcls->add($userAcl);
            $userAcl->setSecurity($this);
        }

        return $this;
    }

    public function removeUserAcl(UserAcl $userAcl): self
    {
        if ($this->userAcls->removeElement($userAcl)) {
            // set the owning side to null (unless already changed)
            if ($userAcl->getSecurity() === $this) {
                $userAcl->setSecurity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, GroupAcl>
     */
    public function getGroupAcls(): Collection
    {
        return $this->groupAcls;
    }

    public function addGroupAcl(GroupAcl $groupAcl): self
    {
        if (!$this->groupAcls->contains($groupAcl)) {
            $this->groupAcls->add($groupAcl);
            $groupAcl->setSecurity($this);
        }

        return $this;
    }

    public function removeGroupAcl(GroupAcl $groupAcl): self
    {
        if ($this->groupAcls->removeElement($groupAcl)) {
            // set the owning side to null (unless already changed)
            if ($groupAcl->getSecurity() === $this) {
                $groupAcl->setSecurity(null);
            }
        }

        return $this;
    }
}
