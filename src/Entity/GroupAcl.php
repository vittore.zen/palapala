<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity()]
#[Gedmo\Loggable]
#[ORM\HasLifecycleCallbacks]
class GroupAcl
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    protected Uuid $id;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, nullable: false)]
    private string $permission;

    /**
     * Many groupAcls have one security. This is the owning side.
     */
    #[ORM\ManyToOne(targetEntity: 'Security', inversedBy: 'groupAcls')]
    #[ORM\JoinColumn(name: 'security_id', referencedColumnName: 'id')]
    private ?Security $security = null;

    /**
     * Many groupAcls have one group. This is the owning side.
     */
    #[ORM\ManyToOne(targetEntity: 'Group', inversedBy: 'groupAcls')]
    #[ORM\JoinColumn(name: 'group_id', referencedColumnName: 'id')]
    private ?Group $group = null;

    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return (string) $this->group.' - '.$this->permission;
    }

    public function getPermission(): ?string
    {
        return $this->permission;
    }

    public function setPermission(string $permission): self
    {
        $this->permission = $permission;

        return $this;
    }

    public function getSecurity(): ?Security
    {
        return $this->security;
    }

    public function setSecurity(?Security $security): self
    {
        $this->security = $security;

        return $this;
    }

    public function getGroup(): ?Group
    {
        return $this->group;
    }

    public function setGroup(?Group $group): self
    {
        $this->group = $group;

        return $this;
    }
}
