<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: \Gedmo\Tree\Entity\Repository\NestedTreeRepository::class)]
#[Gedmo\Tree(type: 'nested')]
#[Gedmo\Loggable]
#[ORM\HasLifecycleCallbacks]
class Folder
{
    /**
     * @var string|null
     */
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    protected $id;



    #[ORM\Column(type: Types::STRING, nullable: false)]
    private string $title;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    /**
     * @var string
     */
    #[Gedmo\Slug(fields: ['title'])]
    #[ORM\Column(length: 128, unique: true)]
    private $slug;

    #[Gedmo\TreeLeft]
    #[ORM\Column(type: Types::INTEGER, nullable: true)]
    private ?int $lft = null;

    #[Gedmo\TreeRight]
    #[ORM\Column(type: Types::INTEGER, nullable: true)]
    private ?int $rgt = null;

    #[ORM\JoinColumn(name: 'folder_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[Gedmo\TreeParent]
    #[ORM\ManyToOne(targetEntity: Folder::class, inversedBy: 'children')]
    private ?Folder $parent = null;

    #[Gedmo\TreeRoot]
    #[ORM\Column(type: Types::STRING, nullable: true)]
    private ?string $root = null;

    #[Gedmo\TreeLevel]
    #[ORM\Column(name: 'lvl', type: Types::INTEGER, nullable: true)]
    private ?int $level = null;

    /**
     * @var Collection<int, Folder>
     */
    #[ORM\OneToMany(targetEntity: Folder::class, mappedBy: 'parent')]
    private Collection $children;

    #[Gedmo\Timestampable(on: 'create')]
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $created = null;

    #[Gedmo\Timestampable(on: 'update')]
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $updated = null;

    /**
     * @var Collection<int, Document>
     */
    #[ORM\OneToMany(targetEntity: Document::class, mappedBy: 'folder')]
    private Collection $documents;

    /**
     * @var Collection<int, Link>
     */
    #[ORM\OneToMany(targetEntity: Link::class, mappedBy: 'folder')]
    private Collection $links;

    /**
     * @var Collection<int,Tag>
     */
    #[ORM\JoinTable(name: 'folders_tags')]
    #[ORM\ManyToMany(targetEntity: Tag::class, inversedBy: 'folders')]
    private Collection $tags;

    /**
     * One Folder has One Security.
     */
    #[ORM\JoinColumn(name: 'security_id', referencedColumnName: 'id')]
    #[ORM\OneToOne(targetEntity: Security::class)]
    private ?Security $security = null;

    #[Gedmo\Timestampable(on: 'create')]
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $createdAt = null;

    #[Gedmo\Timestampable(on: 'update')]
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $updatedAt = null;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->id = (string) Uuid::v4();
        $this->documents = new ArrayCollection();
        $this->links = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    /**
     * Creates a random uuid on persist.
     *
     * @codeCoverageIgnore
     */
    #[ORM\PrePersist]
    public function createId(): void
    {
        $this->id = (string) Uuid::v4();
    }

    public function getId(): ?string
    {
        return (string) $this->id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return trim($this->title);
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getLft(): ?int
    {
        return $this->lft;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setLft(int $lft): self
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getRgt(): ?int
    {
        return $this->rgt;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setRgt(int $rgt): self
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getRoot(): ?string
    {
        return $this->root;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setRoot(?string $root): self
    {
        $this->root = $root;

        return $this;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getLevel(): ?int
    {
        return $this->level;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return ArrayCollection<int, Folder>
     */
    public function getChildren(): ArrayCollection
    {
        $list = [];
        foreach ($this->children as $child) {
            $list[$child->getTitle().(string) $child->getId()] = $child;
        }
        ksort($list);

        return new ArrayCollection(array_values($list));
    }

    public function addChild(Folder $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children->add($child);
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(Folder $child): self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return ArrayCollection<int, Document>
     */
    public function getDocuments(): ArrayCollection
    {
        $list = [];
        foreach ($this->documents as $item) {
            $list[$item->getTitle().(string) $item->getId()] = $item;
        }
        ksort($list);

        return new ArrayCollection(array_values($list));
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents->add($document);
            $document->setFolder($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getFolder() === $this) {
                $document->setFolder(null);
            }
        }

        return $this;
    }

    public function getSecurity(): ?Security
    {
        return $this->security;
    }

    public function setSecurity(?Security $security): self
    {
        $this->security = $security;

        return $this;
    }

    /**
     * @return Collection<int, Link>
     */
    public function getLinks(): Collection
    {
        return $this->links;
    }

    public function addLink(Link $link): static
    {
        if (!$this->links->contains($link)) {
            $this->links->add($link);
            $link->setFolder($this);
        }

        return $this;
    }

    public function removeLink(Link $link): static
    {
        if ($this->links->removeElement($link)) {
            // set the owning side to null (unless already changed)
            if ($link->getFolder() === $this) {
                $link->setFolder(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): static
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
        }

        return $this;
    }

    public function removeTag(Tag $tag): static
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
