<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: 'fos_user')]
#[UniqueEntity('email')]
#[Gedmo\Loggable]
class User implements UserInterface, PasswordAuthenticatedUserInterface, \Stringable
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    protected Uuid $id;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 180, unique: true)]
    #[Gedmo\Versioned]
    private string $username;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 180, unique: true)]
    #[Gedmo\Versioned]
    protected string $email;

    #[ORM\Column(name: 'password', type: \Doctrine\DBAL\Types\Types::STRING, length: 100)]
    #[Gedmo\Versioned]
    protected string $password;

    protected ?string $plainPassword;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 255, nullable: true)]
    protected ?string $passwordRequestToken;

    /**
     * @var array<int,string>
     */
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::ARRAY)]
    #[Gedmo\Versioned]
    private $roles = ['ROLE_USER'];

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, nullable: true)]
    #[Gedmo\Versioned]
    private ?string $firstname;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, nullable: true)]
    #[Gedmo\Versioned]
    private ?string $lastname;

    /**
     * @var array<string,string>|null
     */
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON, nullable: true)]
    #[Gedmo\Versioned]
    protected $preferences;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private ?bool $localAuthentication = true;
    
    #[Gedmo\Timestampable(on: 'create')]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $created = null;

    #[Gedmo\Timestampable(on: 'update')]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $updated = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $lastLogin = null;

    /**
     * @var Collection<int, Group>
     */
    #[ORM\ManyToMany(targetEntity: 'Group', inversedBy: 'users')]
    #[ORM\JoinTable(name: 'users_groups')]
    private Collection $groups;

    /**
     * One user has many userAcl. This is the inverse side.
     *
     * @var Collection<int, UserAcl>
     */
    #[ORM\OneToMany(targetEntity: "App\Entity\UserAcl", mappedBy: 'user')]
    private Collection $userAcls;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->groups = new ArrayCollection();
        $this->userAcls = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->username;
    }

    public function getUserIdentifier(): string
    {
        return $this->username;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): User
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): User
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): User
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function getPasswordRequestToken(): ?string
    {
        return $this->passwordRequestToken;
    }

    public function setPasswordRequestToken(?string $passwordRequestToken): User
    {
        $this->passwordRequestToken = $passwordRequestToken;

        return $this;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param string[] $roles
     *
     * @return $this
     */
    public function setRoles($roles): User
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return string|null The salt
     *
     * @codeCoverageIgnore
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return string The username
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * Removes sensitive data from the user.
     */
    public function eraseCredentials(): void
    {
        $this->plainPassword = null;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(?\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getLastLogin(): ?\DateTimeInterface
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?\DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * @return Collection<int, Group>
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addGroup(Group $group): self
    {
        if (!$this->groups->contains($group)) {
            $this->groups->add($group);
        }

        return $this;
    }

    public function removeGroup(Group $group): self
    {
        $this->groups->removeElement($group);

        return $this;
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    /**
     * @return Collection<int, UserAcl>
     */
    public function getUserAcls(): Collection
    {
        return $this->userAcls;
    }

    public function addUserAcl(UserAcl $userAcl): static
    {
        if (!$this->userAcls->contains($userAcl)) {
            $this->userAcls->add($userAcl);
            $userAcl->setUser($this);
        }

        return $this;
    }

    public function removeUserAcl(UserAcl $userAcl): static
    {
        if ($this->userAcls->removeElement($userAcl)) {
            // set the owning side to null (unless already changed)
            if ($userAcl->getUser() === $this) {
                $userAcl->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getPreferences(): ?array
    {
        return $this->preferences;
    }

    /**
     * @param string[]|null $preferences
     * @return $this
     */
    public function setPreferences(?array $preferences): static
    {
        $this->preferences = $preferences;

        return $this;
    }

    public function isLocalAuthentication(): ?bool
    {
        return $this->localAuthentication;
    }

    public function setLocalAuthentication(?bool $localAuthentication): static
    {
        $this->localAuthentication = $localAuthentication;

        return $this;
    }
}
