<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Table(name: 'fos_group')]
#[ORM\Entity(repositoryClass: "App\Repository\GroupRepository")]
class Group
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    protected Uuid $id;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, nullable: false)]
    private string $name;

    /**
     * Many Groups have Many Users.
     *
     * @var Collection<int, User>
     */
    #[ORM\ManyToMany(targetEntity: 'User', mappedBy: 'groups')]
    private Collection $users;

    /**
     * @var Collection<int, GroupAcl>
     */
    #[ORM\OneToMany(targetEntity: 'GroupAcl', mappedBy: 'group')]
    private Collection $groupAcls;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->users = new ArrayCollection();
        $this->groupAcls = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->addGroup($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeGroup($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, GroupAcl>
     */
    public function getGroupAcls(): Collection
    {
        return $this->groupAcls;
    }

    public function addGroupAcl(GroupAcl $groupAcl): self
    {
        if (!$this->groupAcls->contains($groupAcl)) {
            $this->groupAcls->add($groupAcl);
            $groupAcl->setGroup($this);
        }

        return $this;
    }

    public function removeGroupAcl(GroupAcl $groupAcl): self
    {
        if ($this->groupAcls->removeElement($groupAcl)) {
            // set the owning side to null (unless already changed)
            if ($groupAcl->getGroup() === $this) {
                $groupAcl->setGroup(null);
            }
        }

        return $this;
    }
}
