<?php

namespace App\Command;

use App\Entity\Document;
use App\Entity\Folder;
use App\Entity\Group;
use App\Entity\GroupAcl;
use App\Entity\Security;
use App\Security\PermissionParser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SetPermissionCommand extends Command
{
    protected static $defaultName = 'set-permission';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var PermissionParser
     */
    private $permissionParser;

    public function __construct(EntityManagerInterface $entityManager, PermissionParser $permissionParser)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->permissionParser = $permissionParser;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Set everyone group edit permission')
            ->addOption(
                'type',
                null,
                InputOption::VALUE_REQUIRED,
                'Object type: folder or file',
                null
            )
            ->addOption(
                'id',
                null,
                InputOption::VALUE_REQUIRED,
                'Object ID',
                null
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $object = null;
        if ('folder' == $input->getOption('type')) {
            $object = $this->entityManager->find(Folder::class, $input->getOption('id'));
            if ($object) {
                $output->writeln('Folder: '.$object->getTitle());
            } else {
                $output->writeln('Folder not found.');

                return 1;
            }
        }
        if ('document' == $input->getOption('type')) {
            $object = $this->entityManager->find(Document::class, $input->getOption('id'));
            if ($object) {
                $output->writeln('Document: '.$object->getTitle());
            } else {
                $output->writeln('Document not found.');

                return 1;
            }
        }

        $everyOneGroup = $this->entityManager->getRepository(Group::class)->findOneByName('Everyone');
        if (null == $everyOneGroup) {
            $everyOneGroup = new Group();
            $everyOneGroup->setName('Everyone');
            $this->entityManager->persist($everyOneGroup);
            $this->entityManager->flush();
        }

        $acl = new GroupAcl();
        $acl->setGroup($everyOneGroup);
        $acl->setPermission('edit');
        $this->entityManager->persist($acl);

        $security = new Security();
        $security->addGroupAcl($acl);
        $this->entityManager->persist($security);
        $this->entityManager->flush();
        $object->setSecurity($security);
        $this->entityManager->flush();

        $output->writeln('Permission: '.$this->permissionParser->parse($object, null));

        return 0;
    }
}
