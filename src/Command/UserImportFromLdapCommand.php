<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use LdapRecord\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserImportFromLdapCommand extends Command
{
    protected static $defaultName = 'user:import';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserPasswordHasherInterface
     */
    private $passwordHasher;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordHasher)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->passwordHasher = $passwordHasher;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Import users fromLDAP')
            ->addArgument('ldapServer', InputArgument::REQUIRED)
            ->addArgument('ldapBase', InputArgument::REQUIRED)
            ->addArgument('ldapUsername', InputArgument::REQUIRED)
            ->addArgument('ldapPassword', InputArgument::REQUIRED)
            ->addArgument('emailEnd', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $connection = new Connection([
            'hosts' => [$input->getArgument('ldapServer')],
            'port' => 389,
            'base_dn' => $input->getArgument('ldapBase'),
            'username' => $input->getArgument('ldapUsername'),
            'password' => $input->getArgument('ldapPassword')
        ]);


        $query = $connection->query();
        $query->select(['givenname', 'sn', 'mail'])
            ->where('objectClass', 'person')
            ->where('mail', 'ends_with', $input->getArgument('emailEnd'))
            ->whereHas('givenname')
            ->whereHas('sn');
        $results = $query->get();

        $progressBar = new ProgressBar($output, count($results));

        foreach ($results as $ldapUser) {
            $sn = $ldapUser['sn'][0];
            $givenname = $ldapUser['givenname'][0];
            $mail = $ldapUser['mail'][0];
            $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $mail]);
            if (!$user) {
                $user = new User();
                $user->setUsername($mail);
                $user->setFirstname($givenname);
                $user->setLastname($sn);
                $user->setEmail($mail);
                $hashedPassword = $this->passwordHasher->hashPassword(
                    $user,
                    uuid_create()
                );
                $user->setPassword($hashedPassword);
                $user->setLocalAuthentication(false);
                $this->entityManager->persist($user);
                $this->entityManager->flush();

            } else {
                $user->setFirstname($givenname);
                $user->setLastname($sn);
                $user->setLocalAuthentication(false);
            }

            $progressBar->advance();
        }

        $this->entityManager->flush();


        $progressBar->finish();

        return Command::SUCCESS;
    }
}