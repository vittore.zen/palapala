<?php

namespace App\Command;

use App\Entity\Document;
use App\Entity\Folder;
use App\Entity\Group;
use App\Entity\GroupAcl;
use App\Entity\Security;
use App\Security\PermissionParser;
use App\Service\SmbSync\SyncFoldersPermissions;
use App\Service\SmbSync\SyncFoldersTree;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

#[AsCommand(name: 'smb-sync:folders-tree')]
class SmbSyncFoldersTreeCommand extends Command
{
    public function __construct(
        private string                 $projectDir,
        private SyncFoldersTree $syncFoldersTree)
    {
        parent::__construct();
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $yaml = Yaml::parse(file_get_contents($this->projectDir . '/structure.yaml'));
        $output->writeln('<info>Sync folders tree</info>');
        $this->syncFoldersTree->sync($yaml['folders']);
        return Command::SUCCESS;


    }
}
