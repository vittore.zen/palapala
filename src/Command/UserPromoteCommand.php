<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[\Symfony\Component\Console\Attribute\AsCommand('user:promote', 'Promote an user to admin role')]
class UserPromoteCommand extends Command
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Username');
        $username = $io->ask('Username', null, function ($username) {
            if (empty($username)) {
                throw new \RuntimeException('Username cannot be empty.');
            }

            return $username;
        });

        $user = $this->entityManager->getRepository(\App\Entity\User::class)->findOneBy(['username' => $username]);

        if ($user) {
            $user->setRoles(['ROLE_ADMIN']);
            $this->entityManager->flush();

            $io->success('Utente promosso a ROLE_ADMIN.');

            return Command::SUCCESS;
        } else {
            $io->warning('Utente non promosso a ROLE_ADMIN.');

            return Command::FAILURE;
        }
    }
}
