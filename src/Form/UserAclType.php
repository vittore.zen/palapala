<?php

namespace App\Form;

use App\Entity\Security;
use App\Entity\User;
use App\Entity\UserAcl;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserAclType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('user', EntityType::class, [
                'class' => User::class,
                'choice_label' => 'username',
            ]
            )
            ->add('permission', ChoiceType::class, [
                'choices' => Security::getPermissions(),
                'required' => false,
                'attr' => ['placeholder' => 'Permission'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserAcl::class,
        ]);
    }
}
