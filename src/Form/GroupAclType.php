<?php

namespace App\Form;

use App\Entity\Group;
use App\Entity\GroupAcl;
use App\Entity\Security;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupAclType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('group', EntityType::class, [
                'class' => Group::class,
                'choice_label' => 'name',
            ]
            )
            ->add('permission', ChoiceType::class, [
                'choices' => Security::getPermissions(),
                'required' => false,
                'attr' => ['placeholder' => 'Permission'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => GroupAcl::class,
        ]);
    }
}
