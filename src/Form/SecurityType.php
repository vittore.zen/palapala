<?php

namespace App\Form;

use App\Entity\Security;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SecurityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('groupAcls', CollectionType::class, [
                'entry_type' => GroupAclType::class,
                'allow_delete' => true,
                'allow_add' => true,
                'by_reference' => false,
                'label' => false,
            ])
            ->add('userAcls', CollectionType::class, [
                'entry_type' => UserAclType::class,
                'allow_delete' => true,
                'allow_add' => true,
                'by_reference' => false,
                'label' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Security::class,
        ]);
    }
}
