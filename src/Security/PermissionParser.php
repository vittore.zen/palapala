<?php

namespace App\Security;

use App\Entity\Document;
use App\Entity\Folder;
use App\Entity\Group;
use App\Entity\GroupAcl;
use App\Entity\Link;
use App\Entity\User;
use App\Entity\UserAcl;
use Doctrine\ORM\EntityManagerInterface;

class PermissionParser
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function parse(Folder|Document|Link $object, ?User $user): string
    {
        if ($object instanceof Folder) {
            return $this->parseFolder($object, $user);
        }
        if ($object instanceof Document) {
            return $this->parseDocument($object, $user);
        }
        if ($object instanceof Link) {
            return $this->parseDocument($object, $user);
        }
    }

    private function parseFolder(Folder $folder, ?User $user): string
    {
        $result = $this->parseFolderUser($folder, $user);
        if ($result) {
            return $result;
        }

        $result = $this->parseFolderGroup($folder, $user);
        if ($result) {
            return $result;
        }

        return 'no-access';
    }

    private function parseDocument(Document|Link $document, ?User $user): string
    {
        $result = $this->parseDocumentUser($document, $user);

        if ($result) {
            return $result;
        }
        $result = $this->parseDocumentGroup($document, $user);
        if ($result) {
            return $result;
        }

        return $this->parseFolder($document->getFolder(), $user);
    }

    private function parseFolderGroup(Folder $folder, ?User $user): ?string
    {
        //     echo "\r\n".$folder->getTitle().' -- '.(string)$user;
        $everyone = $this->entityManager->getRepository(Group::class)->findOneBy(['name'=>'Everyone']);
        $authenticatedUsers = $this->entityManager->getRepository(Group::class)->findOneBy(['name'=>'Authenticated users']);

        if (null == $user) {
            $groupAcls = $this->entityManager->getRepository(GroupAcl::class)->findBy(['group' => $everyone, 'security' => $folder->getSecurity()]);
            $groupAcl = ($groupAcls) ? $groupAcls[0] : null;
            if ($groupAcl) {
                return $groupAcl->getPermission();
            }
            if ($folder->getParent()) {
                return $this->parseFolder($folder->getParent(), null);
            }

            return 'no-access';
        }

        $groups = [];

        $groupsFromUsers = $user->getGroups();
        foreach ($groupsFromUsers as $groupFromUser) {
            if (in_array($groupFromUser->getName(), ['Everyone', 'Authenticated users'])) {
                continue;
            }
            $groups[] = $groupFromUser;
        }
        if ($authenticatedUsers) {
            $groups[] = $authenticatedUsers;
        }

        if ($everyone) {
            $groups[] = $everyone;
        }

        foreach ($groups as $group) {
            //          echo "\r\n   - ".$group->getName();
            $groupAcls = $this->entityManager->getRepository(GroupAcl::class)->findBy(['group' => $group, 'security' => $folder->getSecurity()]);
            $groupAcl = ($groupAcls) ? $groupAcls[0] : null;
            if ($groupAcl) {
                //             echo " * ".$groupAcl->getPermission();
                return $groupAcl->getPermission();
            }
        }

        if ($folder->getParent()) {
            return $this->parseFolder($folder->getParent(), $user);
        }

        return null;
    }

    private function parseFolderUser(Folder $folder, ?User $user): ?string
    {
        if (null == $user) {
            return null;
        }

        $userAcl = $this->entityManager->getRepository(UserAcl::class)->findOneBy(['user' => $user, 'security' => $folder->getSecurity()]);
        if ($userAcl) {
            return $userAcl->getPermission();
        }

        return null;
    }

    private function parseDocumentUser(Document|Link $document, ?User $user): ?string
    {
        if (null == $user) {
            return null;
        }

        $userAcl = $this->entityManager->getRepository(UserAcl::class)->findOneBy(['user' => $user, 'security' => $document->getSecurity()]);
        if ($userAcl) {
            return $userAcl->getPermission();
        }

        return null;
    }

    private function parseDocumentGroup(Document|Link $document, ?User $user): ?string
    {
        $everyone = $this->entityManager->getRepository(Group::class)->findOneBy(['name'=>'Everyone']);
        $authenticatedUsers = $this->entityManager->getRepository(Group::class)->findOneBy(['name'=>'Authenticated users']);

        if (null == $user) {
            $groupAcls = $this->entityManager->getRepository(GroupAcl::class)->findBy(['group' => $everyone, 'security' => $document->getSecurity()]);
            $groupAcl = ($groupAcls) ? $groupAcls[0] : null;
            if ($groupAcl) {
                return $groupAcl->getPermission();
            }

            return null;
        }

        $groups = $user->getGroups();
        if ($authenticatedUsers) {
            $groups[] = $authenticatedUsers;
        }
        if ($everyone) {
            $groups[] = $everyone;
        }

        foreach ($groups as $group) {
            $groupAcl = $this->entityManager->getRepository(GroupAcl::class)->findOneBy(['group' => $group, 'security' => $document->getSecurity()]);
            if ($groupAcl) {
                return $groupAcl->getPermission();
            }
        }

        return null;
    }
}
