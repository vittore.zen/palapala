<?php

namespace App\Security\Voter;

use App\Entity\Link;
use App\Security\PermissionParser;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\NullToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class LinkVoter extends Voter
{
    public const VIEW = 'view';
    public const EDIT = 'edit';

    /**
     * @var PermissionParser
     */
    private $permissionParser;

    public function __construct(PermissionParser $permissionParser)
    {
        $this->permissionParser = $permissionParser;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT])) {
            return false;
        }
        if (!$subject instanceof Link) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $link, TokenInterface $token): bool
    {
        switch ($attribute) {
            case self::VIEW:
                return $this->canView($link, $token);
            case self::EDIT:
                return $this->canEdit($link, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(Link $link, TokenInterface $token): bool
    {
        if ($this->canEdit($link, $token)) {
            return true;
        }
        if ($token instanceof NullToken) {
            return 'view' == $this->permissionParser->parse($link, null);
        }

        return 'view' == $this->permissionParser->parse($link, $token->getUser());
    }

    private function canEdit(Link $link, TokenInterface $token): bool
    {
        if ($token instanceof NullToken) {
            return 'edit' == $this->permissionParser->parse($link, null);
        }

        return 'edit' == $this->permissionParser->parse($link, $token->getUser());
    }
}
