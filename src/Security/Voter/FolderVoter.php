<?php

namespace App\Security\Voter;

use App\Entity\Folder;
use App\Security\PermissionParser;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\NullToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class FolderVoter extends Voter
{
    public const VIEW = 'view';
    public const EDIT = 'edit';

    /**
     * @var PermissionParser
     */
    private $permissionParser;

    public function __construct(PermissionParser $permissionParser)
    {
        $this->permissionParser = $permissionParser;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT])) {
            return false;
        }
        if (!$subject instanceof Folder) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $folder, TokenInterface $token): bool
    {
        switch ($attribute) {
            case self::VIEW:
                return $this->canView($folder, $token);
            case self::EDIT:
                return $this->canEdit($folder, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(Folder $folder, TokenInterface $token): bool
    {

        if ($this->canEdit($folder, $token)) {
            return true;
        }
        if ($token instanceof NullToken) {
            return 'view' == $this->permissionParser->parse($folder, null);
        }

        return 'view' == $this->permissionParser->parse($folder, $token->getUser());
    }

    private function canEdit(Folder $folder, TokenInterface $token): bool
    {
        if ($token instanceof NullToken) {
            return 'edit' == $this->permissionParser->parse($folder, null);
        }

        return 'edit' == $this->permissionParser->parse($folder, $token->getUser());
    }
}
