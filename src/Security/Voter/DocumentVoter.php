<?php

namespace App\Security\Voter;

use App\Entity\Document;
use App\Security\PermissionParser;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\NullToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class DocumentVoter extends Voter
{
    public const VIEW = 'view';
    public const EDIT = 'edit';

    /**
     * @var PermissionParser
     */
    private $permissionParser;

    public function __construct(PermissionParser $permissionParser)
    {
        $this->permissionParser = $permissionParser;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT])) {
            return false;
        }
        if (!$subject instanceof Document) {
            return false;
        }

        return true;
    }


    protected function voteOnAttribute(string $attribute, $document, TokenInterface $token): bool
    {
        switch ($attribute) {
            case self::VIEW:
                return $this->canView($document, $token);
            case self::EDIT:
                return $this->canEdit($document, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(Document $document, TokenInterface $token): bool
    {
        if ($this->canEdit($document, $token)) {
            return true;
        }
        if ($token instanceof NullToken) {
            return 'view' == $this->permissionParser->parse($document, null);
        }

        return 'view' == $this->permissionParser->parse($document, $token->getUser());
    }

    private function canEdit(Document $document, TokenInterface $token): bool
    {
        if ($token instanceof NullToken) {
            return 'edit' == $this->permissionParser->parse($document, null);
        }

        return 'edit' == $this->permissionParser->parse($document, $token->getUser());
    }
}
