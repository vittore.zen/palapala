#!/bin/bash

php bin/console smb-sync:groups
php bin/console smb-sync:folders-permissions
for i in {1..5}; do
    php bin/console smb-sync:folders-tree
done
php bin/console smb-sync:files
php bin/console fos:elastic:populate