#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive
sed -i "s/bookworm main/bullseye main contrib/"  /etc/apt/sources.list
apt-get update

echo "Update packages"
apt-get install wget gnupg2 -y
apt-get install ca-certificates apt-transport-https wget -y
apt-get update
apt-get upgrade -y


echo "localhost" > /etc/hostname
hostname --file /etc/hostname


echo "Installing APT packages"
apt-get install lsb-release ca-certificates sudo wget curl git-core build-essential \
  vim elinks zip unzip software-properties-common  graphviz supervisor postgresql \
  autoconf automake build-essential rabbitmq-server nfs-kernel-server -y




echo "Installing postfix"
debconf-set-selections <<<"postfix postfix/mailname string dev.palapala.local"
debconf-set-selections <<<"postfix postfix/main_mailer_type string 'Internet Site'"
apt-get install -y postfix





echo "Configuring postgresQL"
sudo -u postgres psql -U postgres -d postgres -c "alter user postgres with password 'postgres';"
PG_PATH=$(pg_config | grep SHAREDIR | awk -e '{ print $3 }' | sed 's/usr\/share/etc/g')
PG_CONFIG=$PG_PATH/main/postgresql.conf
COUNT=$(grep -c "listen_addresses = '\*'" $PG_CONFIG)
if [ $COUNT -eq 0 ]; then
  echo "listen_addresses = '*'" >>$PG_CONFIG
fi
PG_HBA=$PG_PATH/main/pg_hba.conf
COUNT=$(grep -c "host    all       all   0.0.0.0/0     md5" $PG_HBA)
if [ $COUNT -eq 0 ]; then
  echo "host    all       all   0.0.0.0/0     md5" >>$PG_HBA
fi


echo "Configuring openLiteSpeed"
if [ ! -f /usr/local/lsws/admin/fcgi-bin/admin_php ]; then
    wget -O - http://rpms.litespeedtech.com/debian/enable_lst_debain_repo.sh | bash
    apt install openlitespeed -yq
    systemctl is-enabled lsws
    systemctl status lsws
    apt install lsphp81 lsphp81-common lsphp81-mysql lsphp81-dev lsphp81-curl lsphp81-dev pkg-config \
          build-essential lsphp81-imagick lsphp81-intl  lsphp81-pgsql lsphp81-ldap -yq
    grep -r lsphp74/bin/lsphp /usr/local/lsws/conf -exec  sed -i 's\lsphp74\lsphp81\g' {} \;
    ENCRYPT_PASS=`/usr/local/lsws/admin/fcgi-bin/admin_php -q /usr/local/lsws/admin/misc/htpasswd.php password`
    echo "admin:$ENCRYPT_PASS" > /usr/local/lsws/admin/conf/htpasswd
    find /usr/local/lsws/conf -name 'httpd_config*.*' -exec  sed -i 's\lsphp74\lsphp81\g' {} \;
    find /usr/local/lsws/conf -name 'httpd_config*.*' -exec  sed -i 's\8088\80\g
' {} \;
    find /usr/local/lsws/conf -name 'httpd_config*.*' -exec  sed -i 's\vhRoot *Example\vhRoot /vagrant/public\g' {} \;
    find /usr/local/lsws/conf/vhosts -name 'vhconf*.*' -exec  sed -i 's\VH_ROOT/html/\VH_ROOT/\g' {} \;
    awk '!/rewrite {/' RS= ORS="\n\n" /usr/local/lsws/conf/vhosts/Example/vhconf.conf > /tmp/vhconf.conf
    cat >>  /tmp/vhconf.conf <<ADDTEXT
rewrite  {
  enable                  1
  autoLoadHtaccess        1
  logLevel                0
}
ADDTEXT
    cp /tmp/vhconf.conf /usr/local/lsws/conf/vhosts/Example/vhconf.conf
    systemctl restart lsws
    update-alternatives --install /usr/bin/php php /usr/local/lsws/lsphp81/bin/php8.1 10
    update-alternatives --set php /usr/local/lsws/lsphp81/bin/php8.1
fi


if [ ! -f /usr/local/bin/composer ]; then
  echo "Get composer"
  curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin
  mv /usr/bin/composer.phar /usr/bin/composer
  chmod a+x /usr/bin/composer
fi

if [ ! -f /usr/local/lsws/lsphp81/etc/php/8.1/mods-available/40-amqp.ini ]; then
  apt install  librabbitmq-dev -yq
  cd /tmp
    wget https://pecl.php.net/get/amqp-1.11.0.tgz
      tar xvzf amqp-1.11.0.tgz
      cd amqp-1.11.0/
      /usr/local/lsws/lsphp81/bin/phpize
      ./configure --with-php-config=/usr/local/lsws/lsphp81/bin/php-config
  ./configure --with-php-config=/usr/local/lsws/lsphp81/bin/php-config --with-amqp=/usr/local/lsws/lsphp81
  make
  make install
  ls   /usr/local/lsws/lsphp81/lib/php/20210902/
  echo "extension=amqp.so" > /usr/local/lsws/lsphp81/etc/php/8.1/mods-available/40-amqp.ini
  killall lsphp;killall lsphp;killall lsphp;
  systemctl restart lsws
fi




wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo gpg --dearmor -o /usr/share/keyrings/elasticsearch-keyring.gpg
apt-get install apt-transport-https -yq
echo "deb [signed-by=/usr/share/keyrings/elasticsearch-keyring.gpg] https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
apt-get update && apt-get install elasticsearch hunspell-it -yq
systemctl daemon-reload
systemctl enable elasticsearch.service
systemctl start elasticsearch.service
mkdir -p /etc/elasticsearch/hunspell/it_IT
cp /usr/share/hunspell/it_IT* /etc/elasticsearch/hunspell/it_IT


mkdir /dev/shm/sf
chmod -R 777 /dev/shm/sf

systemctl restart lsws

apt-get install cachefilesd
sudo echo "RUN=yes" > /etc/default/cachefilesd
